[![introduction](doc/img/introduction.png)](doc/src/introduction.comic)

Code Comic
==========
Code Comic is a domain-specific language and Go library for generating web comics from a markup-like language. This is similar to how [Graphvis](https://graphviz.org/) is a domain-specific language for making graph visualizations and [OpenSCAD](https://openscad.org/) is a domain-specific language for making 3d models. (Code Comic isn't affiliated with either one, but they're both worth checking out if you have use cases which might benefit from those tools.)

Current Status
--------------
Code Comic is still in early beta and no guarantees are made currently regarding backwards compatibility of future versions.

There may be bugs (please do report any you find) and the the DSL syntax does not have a formal definition yet.

Many basic features are not present such as Microsoft Windows support, and a richer set of command line flags. The Go api is also lacking and Code Comic doesn't yet allow plugins.

However, it is functional and I am using it for real-world applications. (Specifically DM notes for a Dungeons and Dragons campaign.)

DSL Introduction
----------------
The following is a very simple complete program for generating a very simple comic. The "Hello World" of Code Comic if you will:

```
include "_codecomic/skeletons/simple_person.comic"
include "_codecomic/poses/neutral.comic"

layout {
	frame {
		object {
			kind = "stick"
			skel = simple_person
			poses = list { pose neutral }
		}
	}
}
```

The above source code, when executed, will produce the following image:

[![Hello World Comic](doc/img/hello_world.png)](doc/src/hello_world.comic)

The first two lines pull in values from files. Specifically from files built into the Code Comic executable. More details on builtins later. But the include directive can also pull values from files on the local filesystem (relative to the current working directory.)

Some source files only define values to be pulled into other files. Any file which is intended to be executed to generate a comic must have as its last top-level declaration a layout. The layout decides the positioning of the frames in the comic. The default kind of layout arranges frames in rows limiting to a maximum width of 800 pixels.

The layout contains one or more frames. Each defaults to 200 by 200 pixels and contains objects. Objects are things which may be drawn in a frame such as text, an image, or in this case, a stick figure. The "kind" parameter decides what sort of object it is.

"stick"-type objects require a skeleton. While poses are not technically required, a skeleton without any poses will typically not be posed sensically. It will be posed in a way that's easy to pose, but doesn't display well. Much like 3D models may default to a "t-pose."

The skeleton "simple_person" comes from the imported builtin file "_codecomic/skeletons/simple_person.comic" and represents a human(oid) with two legs and two arms, each jointed at a knee or elbow respectively, a torso, and a head. The "neutral" pose from the builtin file "_codecomic/poses/neutral.comic" and represents a standing, at rest position.

Aside from the "kind", "skel", and "poses" parameters, "stick"-type objects have multiple other optional parameters which allow customizing the color, location, or scale of the stick figure, or attaching other objects to the stick figure.

A frame draws all the objects it contains, cropped and zoomed such that its contents are as large as possible without affecting the aspect ratio of the frame or the contents.

More documentation of the DSL is planned, but for now if you want to see more complex code examples, click the comic at the very top of this README or explore the source code of some of the builtins in the [assets directory](assets).

Running/Building/Hacking
------------------------
Running and building Code Comic are simple processes. It requires that Go be installed. (Version 1.18 or later.)

Clone the repository and from the repository directory, run the following:

```
go get ./main
```

This will download all dependencies needed to build Code Comic.

To run Code Comic without building an executable first, run:

```
go run ./main
```

To build it into an executable which can be run without go, run the following:

```
go build -o codecomic ./main
```

If you make any changes to the assets directory and want those changes to be present in the builtins of the built executable, you'll need to use the `go generate` command to "transpile" the assets into go source code before running/building:

```
go generate ./resources/assets
```

Running unit tests can be done with the following:

```
go test ./...
```

Legal Stuff
-----------
Copyright 2022 AntiMS

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received  a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
