package lex

import (
	"fmt"
	"io"
)

const (
	SKIPCUR = iota
	SKIPPREV
	DONTSKIP
)

var tokenAbbrevRev map[uint]rune = make(map[uint]rune)

func init() {
	for r, i := range tokenAbbrev {
		tokenAbbrevRev[i] = r
	}
}

func toString(token Token) string {
	out, ok := tokenAbbrevRev[token.Kind]
	if ok {
		return string(out)
	}
	switch token.Kind {
	// Any omitted from this list are present in the tokenAbbrevRev list.
	case UNKNOWN:
		panic("Unknown token during output")
	case SEMICOLON:
		return ";"
	case NEWLINE:
		return ";" // For minifying
	case OPENBLOCK:
		return "{"
	case CLOSEBLOCK:
		return "}"
	case EQUALS:
		return "="
	case IDENTIFIER:
		return token.Value.(string)
	case NUMBER:
		fallthrough
	case STRING:
		return fmt.Sprintf("%#v", token.Value)
	}
	panic(fmt.Sprintf("Invalid token %d during output", token.Kind))
}

func skipToken(prev, cur uint) uint {
	prevsep := (prev == NEWLINE || prev == SEMICOLON || prev == OPENBLOCK)
	if (cur == NEWLINE || cur == SEMICOLON) && prevsep {
		return SKIPCUR
	} else if prev == UNKNOWN || (cur == CLOSEBLOCK && prevsep) {
		return SKIPPREV
	}
	return DONTSKIP
}

// Writes tokens received from the given channel in a space-efficient binary format.
// This format replaces all reserved keywords longer than one character ("point", "color", "layout", "object", etc) with one-byte binary identifiers and removes unnecessary whitespace.
func Output(writer io.StringWriter, ts <-chan Token) {
	prev := Token{}
	for token := range ts {
		skip := skipToken(prev.Kind, token.Kind)
		if skip == SKIPCUR {
			continue
		}
		if skip != SKIPPREV {
			_, err := writer.WriteString(toString(prev))
			if err != nil {
				panic(err)
			}
		}
		prev = token
	}
	if prev.Kind != NEWLINE && prev.Kind != SEMICOLON {
		_, err := writer.WriteString(toString(prev))
		if err != nil {
			panic(err)
		}
	}
}
