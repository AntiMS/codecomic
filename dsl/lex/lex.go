// The lex package contains a hand-written lexer for the CodeComic domain-specific language.
package lex

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

// Token types
const (
	UNKNOWN = iota

	// Special
	INCLUDE

	// Block types
	SKELETON
	BONE
	POSE
	OBJECT
	FRAME
	POINT
	LAYOUT
	COLOR
	LIST

	// Control
	SEMICOLON
	NEWLINE
	OPENBLOCK
	CLOSEBLOCK

	// Expression stuff
	EQUALS // For assignment only, no comparators
	NUMBER // Value: the number as a float64 ; Weird caveat: .25 is not valid. Must be 0.25 .
	STRING // Value: the value of the string as a string
	TRUE
	FALSE

	// Identifier
	IDENTIFIER // Value: the name of the identifier as a string
)

// One-byte binary forms of reserved keywords. Used in parsing the space-efficient binary format.
var tokenAbbrev map[rune]uint = map[rune]uint{
	'\x01': INCLUDE,
	'\x02': SKELETON,
	'\x03': BONE,
	'\x04': POSE,
	'\x05': OBJECT,
	'\x06': FRAME,
	'\x07': POINT,
	'\x08': LAYOUT,
	// 0x09 and 0x0a are tab and newline respectively.
	'\x0b': COLOR,
	'\x0c': LIST,
	// 0x0d is a carriage return.
	'\x0e': TRUE,
	'\x0f': FALSE,
}

// Map of human-readable names for token types.
var tokenKinds map[uint]string = map[uint]string{
	UNKNOWN:    "unknown",
	INCLUDE:    "include",
	SKELETON:   "skeleton",
	BONE:       "bone",
	POSE:       "pose",
	OBJECT:     "object",
	FRAME:      "frame",
	POINT:      "point",
	LAYOUT:     "layout",
	COLOR:      "color",
	LIST:       "list",
	SEMICOLON:  "semicolon",
	NEWLINE:    "newline",
	OPENBLOCK:  "openblock",
	CLOSEBLOCK: "closeblocK",
	EQUALS:     "equals",
	NUMBER:     "number",
	STRING:     "string",
	TRUE:       "true",
	FALSE:      "false",
	IDENTIFIER: "identifier",
}

const CHAN_SIZE = 256

var ignoredWhitespace = " \t"
var tokenEnders = ignoredWhitespace + "\n;{}#,=\"" // Windows-style CRLF line endings are not supported for now
var identifierChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"

// A CodeComic domain-specific language token.
type Token struct {
	Kind  uint
	Value interface{}
	Line  uint
}

// Given a token type, returns a human-readable name of that token type.
func SerializeKind(k uint) string {
	return tokenKinds[k]
}

// Returns a human-readable representation of this token.
func (t Token) Serialize() string {
	out := "T:" + strconv.Itoa(int(t.Line)) + ":" + SerializeKind(t.Kind)
	if t.Value != nil {
		out += ":" + fmt.Sprintf("%#v", t.Value)
	}
	return out
}

// If the given rune represents an entire token, returns that token and true. A zero-value token and false, otherwise.
func getOneRuneToken(t rune) (Token, bool) {
	out, ok := tokenAbbrev[t]
	if ok {
		return Token{Kind: out}, true
	}
	switch t {
	case '\n':
		return Token{Kind: NEWLINE}, true
	case '{':
		return Token{Kind: OPENBLOCK}, true
	case '}':
		return Token{Kind: CLOSEBLOCK}, true
	case '=':
		return Token{Kind: EQUALS}, true
	case ';':
		return Token{Kind: SEMICOLON}, true
	}
	return Token{}, false
}

// Given a string, returns the token it parses to.
func getToken(t string) Token {
	switch t {
	case "":
		panic("Empty token received")
	case "include":
		return Token{Kind: INCLUDE}
	case "skeleton":
		return Token{Kind: SKELETON}
	case "bone":
		return Token{Kind: BONE}
	case "pose":
		return Token{Kind: POSE}
	case "object":
		return Token{Kind: OBJECT}
	case "frame":
		return Token{Kind: FRAME}
	case "point":
		return Token{Kind: POINT}
	case "layout":
		return Token{Kind: LAYOUT}
	case "color":
		return Token{Kind: COLOR}
	case "list":
		return Token{Kind: LIST}
	case "true":
		return Token{Kind: TRUE}
	case "false":
		return Token{Kind: FALSE}
	}

	str, err := strconv.Unquote(t)
	if err == nil {
		return Token{Kind: STRING, Value: str}
	}

	flt, err := strconv.ParseFloat(t, 64)
	if err == nil {
		return Token{Kind: NUMBER, Value: flt}
	}

	idr := ([]rune(t)[0] < '0' || []rune(t)[0] > '9')
	for _, c := range []rune(t) {
		idr = idr && strings.ContainsRune(identifierChars, c)
		if !idr {
			break
		}
	}
	if idr {
		return Token{Kind: IDENTIFIER, Value: t}
	}

	if len(t) == 1 { // In theory should never be true
		ort, ok := getOneRuneToken([]rune(t)[0])
		if ok {
			return ort
		}
	}

	panic(fmt.Sprintf("Failed to parse token: %#v", t))
}

// Parses a token from ts, runs f on that token, and then sends the token to c.
func sendToken(ts string, c chan<- Token, f func(t *Token)) {
	if ts == "" {
		return
	}
	t := getToken(ts)
	f(&t)
	c <- t
}

// Returns a function which "decorates" a token with the correct line number.
func getDecorateToken() func(t *Token) {
	var line uint = 1
	return func(t *Token) {
		t.Line = line
		if t.Kind == NEWLINE {
			line++
		}
	}
}

func lex(r *bufio.Reader, c chan<- Token) {
	decorateToken := getDecorateToken()
	current := ""
	for {
		rn, _, err := r.ReadRune()
		if err == io.EOF {
			sendToken(current, c, decorateToken)
			close(c)
			break
		} else if err != nil {
			panic(err)
		}

		if strings.ContainsRune(tokenEnders, rn) {
			sendToken(current, c, decorateToken)
			current = ""
			if rn == '#' {
				for tmp, _, err := r.ReadRune(); tmp != '\r' && tmp != '\n'; tmp, _, err = r.ReadRune() {
					if err != nil {
						panic(err)
					}
				}
				tosend := Token{Kind: NEWLINE}
				decorateToken(&tosend)
				c <- tosend
			} else if rn == '"' {
				current = "\""
				for tmp, _, err := r.ReadRune(); tmp != '"' || current[len(current)-1] == '\\'; tmp, _, err = r.ReadRune() {
					if err != nil {
						panic(err)
					}
					current += string(tmp)
				}
				current += "\""
				sendToken(current, c, decorateToken)
				current = ""
			} else if rn == '-' || (rn >= '0' && rn <= '9') || (rn == '.' && current[0] >= '0' && current[0] <= '9') {
				current += string(rn)
			} else if strings.ContainsRune(ignoredWhitespace, rn) {
				// Intentionally blank
			} else {
				ort, ok := getOneRuneToken(rn)
				if ok {
					decorateToken(&ort)
					c <- ort
				} else {
					current += string(rn)
				}
			}
		} else {
			if current == "" {
				ort, ok := getOneRuneToken(rn)
				if ok {
					decorateToken(&ort)
					c <- ort
					continue
				}
			}
			current += string(rn)
		}
	}
}

// Spawns a goroutine which lexes the CodeComic domain-specific language code read from r. Returns a channel from which tokens are to be read.
func Lex(r io.Reader) <-chan Token {
	out := make(chan Token, CHAN_SIZE)
	go lex(bufio.NewReader(r), (chan<- Token)(out))
	return (<-chan Token)(out)
}
