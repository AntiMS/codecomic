package runtime

import (
	"image"
	"io"

	"gitlab.com/AntiMS/codecomic/dsl/expr"
	"gitlab.com/AntiMS/codecomic/dsl/lex"
	"gitlab.com/AntiMS/codecomic/dsl/parse"
)

// Handles some miscellaneous initialization and coordination for turning CodeComic DSL code from an io.Reader into an image.
// Lexes and parses, executes the expressions, finds the layout (the last global variable), creates a new image, and draws all of the frames in the layout on the image before returning it.
func Run(in io.Reader) image.Image {
	exprs, order := parse.Parse(lex.Lex(in))
	globals := make(map[string]interface{})

	var layout *expr.LayoutInfo
	for i, n := range order {
		globals[n] = exprs[n].Eval()
		lv, ok := globals[n].(*expr.LayoutInfo)
		if ok {
			if i != len(order)-1 {
				panic("Extra blocks after layout")
			}
			layout = lv
		}
	}
	if layout == nil {
		panic("Could not find a layout in the global space")
	}

	out := image.NewRGBA(layout.ImageBounds)

	for _, f := range layout.Frames {
		f.Draw(out)
	}

	return out
}
