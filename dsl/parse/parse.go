package parse

import (
	"encoding/base64"
	"fmt"
	"math/rand"

	"gitlab.com/AntiMS/codecomic/dsl/expr"
	"gitlab.com/AntiMS/codecomic/dsl/lex"
	"gitlab.com/AntiMS/codecomic/resources"
)

const (
	CONT = iota
	EOF
	CLOSE
)

// Parses an included file and returns its global scope and top-level delaration order.
func parseIncludeByPath(path string, scope map[string]expr.Expr, exprs map[string]expr.Expr, order []string) (map[string]expr.Expr, []string) {
	in, ok := resources.Get(path)
	if !ok {
		panic(fmt.Sprintf("Could not open included file %#v", path))
	}
	defer in.Close()

	pexprs, porder, _ := parseAssignments(lex.Lex(in), scope)
	for _, n := range porder {
		_, ok = exprs[n]
		if !ok {
			order = append(order, n)
		}
		exprs[n] = pexprs[n]
		scope[n] = pexprs[n]
	}

	return exprs, order
}

// Parses a single include directive from the given token channel and returns the included file's global scope and top-level declaration order.
func parseInclude(ts <-chan lex.Token, scope map[string]expr.Expr, exprs map[string]expr.Expr, order []string) (map[string]expr.Expr, []string) {
	pt, ok := <-ts
	if !ok {
		panic("Unexpected EOF immediately after include keyword")
	} else if pt.Kind != lex.STRING {
		panic("Include followed by non-string-literal")
	}

	path := pt.Value.(string)

	tmp, ok := <-ts
	if ok && tmp.Kind != lex.NEWLINE && tmp.Kind != lex.SEMICOLON {
		panic(fmt.Sprintf("Include directive %#v not followed by newline or semicolon", path))
	}

	return parseIncludeByPath(path, scope, exprs, order)
}

// Parses an expression from the given channel token. Panics on syntax errors. Called right after an assignment operator.
func parseExpr(ts <-chan lex.Token, scope map[string]expr.Expr) expr.Expr {
	token, ok := <-ts
	if !ok {
		panic("Missing expression")
	}
	for token.Kind == lex.NEWLINE {
		token, ok = <-ts
		if !ok {
			panic("Missing expression")
		}
	}
	switch token.Kind {
	case lex.IDENTIFIER:
		out, ok := scope[token.Value.(string)]
		if !ok {
			panic(fmt.Sprintf("Reference to nonexistent variable %#v", token.Value.(string)))
		}
		return out
	case lex.STRING:
		return expr.StringLiteral(token.Value.(string))
	case lex.NUMBER:
		return expr.NumberLiteral(token.Value.(float64))
	case lex.TRUE:
		return expr.BooleanLiteral(true)
	case lex.FALSE:
		return expr.BooleanLiteral(false)
	case lex.SKELETON, lex.BONE, lex.POSE, lex.OBJECT, lex.FRAME, lex.POINT, lex.LAYOUT, lex.COLOR, lex.LIST:
		b, ok := <-ts
		if !ok {
			panic("EOF when parsing a block expression")
		}
		if b.Kind != lex.OPENBLOCK {
			panic("Missing open block when parsing a block expression")
		}
		exprs, order, _ := parseAssignments(ts, scope)
		return &expr.Block{Exprs: exprs, Order: order, Kind: token.Kind}
	default:
		panic(fmt.Sprintf("Unexpected token %s when parsing an expession", token.Serialize()))
	}
}

// Generates a random variable name for variables with unspecified names. Such as "point = bob { x = 10 }".
func generateName() string {
	size := 32
	out := make([]byte, size, size)
	n, err := rand.Read(out)
	if err != nil {
		panic("Error generating random parameter name: %s" + err.Error())
	}
	if n < size {
		panic("Error generating random parameter name")
	}
	return "_e" + base64.RawStdEncoding.EncodeToString(out)
}

// Parses specifically a block declaration of the form `{skeleton,pose,object,...} [name] ["="] [from]`
func parseBlock(kind uint, ts <-chan lex.Token) (name string, fromName string, endKind uint) {
	token, ok := <-ts
	if !ok {
		panic(fmt.Sprintf("Unexpected EOF in block assignment after %s", lex.SerializeKind(token.Kind)))
	}

	if token.Kind == lex.IDENTIFIER {
		name = token.Value.(string)
		token, ok = <-ts
		if !ok {
			// In *theory* could be considered valid in some cases, but it's pretty janky.
			panic(fmt.Sprintf("Unexpected EOF in block assignment after identifier %s", name))
		}
	}

	if token.Kind == lex.IDENTIFIER {
		panic("Unexpected second identifier in a row in block assignment")
	} else if token.Kind == lex.EQUALS {
		token, ok = <-ts
		if !ok {
			panic("Unexpected EOF in block assignment after '='")
		}
	}

	if token.Kind == lex.IDENTIFIER {
		fromName = token.Value.(string)
		token, ok = <-ts
		if !ok {
			panic(fmt.Sprintf("Unexpected EOF after identifier from identifier %s", fromName))
		}
	}

	if name == "" {
		name = generateName()
	}

	return name, fromName, token.Kind
}

// Given a scope, fetches from that scope a block of the given kind. If the name does not exist in the scope, returns nil and false. If it does exist but isn't a block or is a block of the wrong kind, panics.
func blockFromScope(name string, kind uint, scope map[string]expr.Expr) (*expr.Block, bool) {
	val, ok := scope[name]
	if !ok {
		return nil, false
	}

	block, ok := val.(*expr.Block)
	if !ok {
		panic(fmt.Sprintf("Outer scope value %s is not a block, is %#v", name, val))
	}

	if block.Kind != kind {
		panic(fmt.Sprintf("Outer scope block %s has kind %s but expected %s", name, lex.SerializeKind(block.Kind), lex.SerializeKind(kind)))
	}

	return block, true
}

// Parses a single assignment of the form "identifier = <value>" or "keyword identifier = identifier { ... }" from the token channel.
func parseAssignment(token lex.Token, ts <-chan lex.Token, scope map[string]expr.Expr) (string, expr.Expr, uint) {
	switch token.Kind {
	case lex.IDENTIFIER:
		equals, ok := <-ts
		if !ok {
			panic("Unexpected EOF after identifier in assigment")
		}
		if equals.Kind != lex.EQUALS {
			panic("Identifier in block not followed by equals sign")
		}
		out := parseExpr(ts, scope)
		tmp, ok := <-ts
		var cont uint = CONT
		if !ok {
			cont = EOF
		} else if tmp.Kind == lex.CLOSEBLOCK {
			cont = CLOSE
		} else if tmp.Kind != lex.NEWLINE && tmp.Kind != lex.SEMICOLON {
			panic(fmt.Sprintf("Assignment '%s' not followed by newline or semicolon", token.Value.(string)))
		}
		return token.Value.(string), out, cont
	case lex.SKELETON, lex.BONE, lex.POSE, lex.OBJECT, lex.FRAME, lex.POINT, lex.LAYOUT, lex.COLOR, lex.LIST:
		name, fromName, endKind := parseBlock(token.Kind, ts)

		originalFromName := fromName
		if fromName == "" {
			fromName = name
		}
		outerBlock, ok := blockFromScope(fromName, token.Kind, scope)
		if originalFromName != "" && !ok {
			panic(fmt.Sprintf("Referenced outer block %s not found or not a block", fromName))
		}

		var block *expr.Block
		var cont uint = CONT
		switch endKind {
		case lex.OPENBLOCK:
			tmpscope := make(map[string]expr.Expr)
			for k, v := range scope {
				tmpscope[k] = v
			}
			if outerBlock != nil {
				for _, n := range outerBlock.Order {
					tmpscope[n] = outerBlock.Exprs[n]
				}
			}
			exprs, order, _ := parseAssignments(ts, tmpscope)
			block = &expr.Block{Exprs: exprs, Order: order, Kind: token.Kind}
			tmp, ok := <-ts
			if !ok {
				cont = EOF
			} else if tmp.Kind == lex.CLOSEBLOCK {
				cont = CLOSE
			} else if tmp.Kind != lex.NEWLINE && tmp.Kind != lex.SEMICOLON {
				panic(fmt.Sprintf("Assignment %s not followed by newline or semicolon", token.Value.(string)))
			}
		case lex.CLOSEBLOCK:
			cont = CLOSE
			fallthrough
		case lex.NEWLINE, lex.SEMICOLON, lex.UNKNOWN:
			if outerBlock == nil {
				panic(fmt.Sprintf("Reference to non-existent variable %#v in outer scope", name))
			}
			block = &expr.Block{Exprs: map[string]expr.Expr{}, Order: []string{}, Kind: token.Kind}
		default:
			panic(fmt.Sprintf("Illegal token kind %s after block assignment declaration for %s", lex.SerializeKind(endKind), name))
		}

		if outerBlock != nil {
			block = outerBlock.Merged(block)
		}

		return name, block, cont
	case lex.CLOSEBLOCK:
		return "", nil, CLOSE
	default:
		panic("Unexpected token in assignments: " + token.Serialize())
	}
}

// Parses all includes and assignments from the given token channel. Returns the scope, the order of declarations, and a boolean which is true if the scope-ending token was an EOF.
func parseAssignments(ts <-chan lex.Token, scope map[string]expr.Expr) (map[string]expr.Expr, []string, bool) {
	tmpscope := make(map[string]expr.Expr)
	for k, v := range scope {
		tmpscope[k] = v
	}
	scope = tmpscope

	exprs := make(map[string]expr.Expr)
	order := make([]string, 0)

	var cont uint = CONT
	for cont == CONT {
		token, ok := <-ts
		if !ok {
			cont = EOF
		}
		if token.Kind == lex.CLOSEBLOCK {
			cont = CLOSE
		}
		for token.Kind == lex.NEWLINE || token.Kind == lex.SEMICOLON {
			token, ok = <-ts
			if !ok {
				cont = EOF
			}
			if token.Kind == lex.CLOSEBLOCK {
				cont = CLOSE
			}
		}
		if cont != CONT {
			break
		}

		if token.Kind == lex.INCLUDE {
			exprs, order = parseInclude(ts, scope, exprs, order)
			continue
		}

		var name string
		var ex expr.Expr
		name, ex, cont = parseAssignment(token, ts, scope)
		if ex != nil {
			if _, in := exprs[name]; in {
				panic(fmt.Sprintf("Parameter %#v redeclared in block", name))
			}
			exprs[name] = ex
			order = append(order, name)
			scope[name] = ex
		}
	}

	return exprs, order, cont == EOF
}

// Parses the CodeComic domain-specif language from a channel of lexer tokens. Returns the global scope and declaration order.
func Parse(ts <-chan lex.Token) (map[string]expr.Expr, []string) {
	exprs, order, eof := parseAssignments(ts, make(map[string]expr.Expr))
	if !eof {
		panic("Unexpected trailing curly braces at end of file")
	}
	return exprs, order
}
