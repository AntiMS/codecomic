package expr

import (
	"fmt"
	"gitlab.com/AntiMS/codecomic/layout"
	"gitlab.com/AntiMS/codecomic/layout/rows"
)

func init() {
	RegisterLayoutKind("rows", []string{"width"}, func(parameters map[string]interface{}) layout.Layout {
		var wi uint = 800
		w, ok := parameters["width"]
		if ok {
			wfi, ok := w.(float64)
			if !ok {
				panic(fmt.Sprintf("Non-number width %#v in rows layout", w))
			}
			wi = uint(wfi)
		}
		return rows.New(wi)
	})
}
