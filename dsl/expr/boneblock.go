package expr

import (
	"fmt"
	"reflect"

	"gitlab.com/AntiMS/codecomic/dsl/lex"
	"gitlab.com/AntiMS/codecomic/point"
)

type Bone struct {
	From *string
	To   *point.PolarPoint
}

func init() {
	blockHandlers[lex.BONE] = &blockHandler{
		value: func(vals map[string]interface{}, order []string) interface{} {
			f, fin := vals["from"]
			t, tin := vals["to"]

			var sf *string
			if fin {
				tsf, ok := f.(string)
				if !ok {
					panic(fmt.Sprintf("Parameter \"from\" value %#v is not a string", f))
				}
				sf = &tsf
			}

			if !tin {
				panic("Required parameter \"to\" in bone not present")
			}
			pt, ok := t.(*point.PolarPoint)
			if !ok {
				panic(fmt.Sprintf("Paramter \"to\" value %#v is not a polar point", t))
			}

			return &Bone{From: sf, To: pt}
		},
		vtype: reflect.TypeOf(&Bone{}),
	}
}
