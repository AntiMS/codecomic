package expr

import (
	"fmt"
	"reflect"

	"gitlab.com/AntiMS/codecomic/dsl/lex"
	"gitlab.com/AntiMS/codecomic/point"
)

func init() {
	panicFloat := func(ok bool, n string, v interface{}) {
		if !ok {
			panic(fmt.Sprintf("Point parameter %s value %#v is not a number", n, v))
		}
	}

	blockHandlers[lex.POINT] = &blockHandler{
		value: func(vals map[string]interface{}, order []string) interface{} {
			x, xin := vals["x"]
			y, yin := vals["y"]
			r, rin := vals["r"]
			t, tin := vals["theta"]
			if (xin || yin) && (rin || tin) {
				panic("Invalid point has both parametric and polar values")
			}
			if !xin && !yin && !rin && !tin {
				panic("Points must have one of \"x\", \"y\", \"r\", \"theta\"")
			}
			par := (xin || yin)
			var out point.Point
			if par {
				var fx, fy float64
				var ok bool
				if xin {
					fx, ok = x.(float64)
					panicFloat(ok, "x", x)
				}
				if yin {
					fy, ok = y.(float64)
					panicFloat(ok, "y", y)
				}
				out = point.NewParametric(fx, fy)
			} else {
				var fr, ft float64
				var ok bool
				if rin {
					fr, ok = r.(float64)
					panicFloat(ok, "r", r)
				} else {
					fr = 1
				}
				if tin {
					ft, ok = t.(float64)
					panicFloat(ok, "theta", t)
				}
				out = point.NewPolar(fr, ft)
			}
			delete(vals, "x")
			delete(vals, "y")
			delete(vals, "r")
			delete(vals, "theta")
			if len(vals) > 0 {
				panic("Point has parameters with invalid names")
			}
			return out
		},
		vtype: reflect.TypeOf((*point.Point)(nil)).Elem(),
	}
}
