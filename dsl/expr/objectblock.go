package expr

import (
	"fmt"
	"reflect"

	"gitlab.com/AntiMS/codecomic/dsl/lex"
	"gitlab.com/AntiMS/codecomic/object"
)

var objectHandlers map[string]func(vals map[string]interface{}, order []string) object.Object = make(map[string]func(vals map[string]interface{}, order []string) object.Object)

func init() {
	blockHandlers[lex.OBJECT] = &blockHandler{
		value: func(vals map[string]interface{}, order []string) interface{} {
			k, ok := vals["kind"]
			if !ok {
				panic("Required parameter \"kind\" not present in object")
			}
			sk, ok := k.(string)
			if !ok {
				panic(fmt.Sprintf("Parameter \"kind\" value %#v in object is not a string", k))
			}

			delete(vals, "kind")
			for i, _ := range order {
				if order[i] == "kind" {
					order = append(order[:i], order[i+1:]...)
					break
				}
			}

			h := objectHandlers[sk]

			return h(vals, order)
		},
		vtype: reflect.TypeOf((*object.Object)(nil)).Elem(),
	}
}
