package expr

import (
	"reflect"

	"gitlab.com/AntiMS/codecomic/dsl/lex"
)

func init() {
	blockHandlers[lex.LIST] = &blockHandler{
		value: func(vals map[string]interface{}, order []string) interface{} {
			out := make([]interface{}, 0, len(order))
			for _, n := range order {
				out = append(out, vals[n])
			}
			return out
		},
		vtype: reflect.TypeOf([]interface{}{}),
	}
}
