package expr

import (
	"fmt"
	"reflect"

	"gitlab.com/AntiMS/codecomic/dsl/lex"
	"gitlab.com/AntiMS/codecomic/point"
)

// An expr.Expr which on Eval()'ing sums points from two wrapped expr.Expr's.
type pointSum struct {
	a, b Expr
}

// Evals both wrapped expr.Expr's and, assuming both returns points, returns the sum thereof. Otherwise panics.
func (o *pointSum) Eval() interface{} {
	ea := o.a.Eval()
	pa, ok := ea.(point.Point)
	if !ok {
		panic("Point sum first argument is not a point")
	}

	eb := o.b.Eval()
	pb, ok := eb.(point.Point)
	if !ok {
		panic("Point sum second argument is not a point")
	}

	return pa.Add(pb)
}

// Returns lex.POINT .
func (o *pointSum) ReturnKind() uint {
	return lex.POINT
}

// An expr.Expr which Eval()'s into a complex type such as an object, frame, point, list, etc.
type Block struct {
	// Parameters in this block.
	Exprs map[string]Expr

	// The order of parameters in this block.
	Order []string

	// Kind value representing what sort of object is to be returned from this Block's Eval(). See the lex package for Kind values.
	Kind uint
}

func (b *Block) normalize() {
	if b.Exprs == nil {
		b.Exprs = make(map[string]Expr)
	}
	if b.Order == nil {
		b.Order = make([]string, 0)
	}
}

func (b *Block) vals() (map[string]interface{}, []string) {
	b.normalize()
	vals := make(map[string]interface{})
	order := make([]string, 0)
	for _, n := range b.Order {
		v := b.Exprs[n].Eval()
		vals[n] = v
		order = append(order, n)
	}
	return vals, order
}

func merge(a, b Expr) Expr {
	ta, ok := a.(interface {
		Expr
		ReturnKind() uint
	})
	if !ok {
		return b
	}
	tb, ok := b.(interface {
		Expr
		ReturnKind() uint
	})
	if !ok {
		return b
	}

	if ta.ReturnKind() != tb.ReturnKind() {
		return b
	}

	if ta.ReturnKind() == lex.POINT {
		return &pointSum{a, b}
	}

	return b
}

// Returns a block which is the merger of the receiving block and the given block. For most block types, this is just the receiving block with parameters from the given block overriding. For points, it is the sum of the points.
func (b *Block) Merged(t *Block) *Block {
	if b.Kind != t.Kind {
		panic(fmt.Sprintf("Cannot merge blocks of two different types: %s and %s", lex.SerializeKind(b.Kind), lex.SerializeKind(t.Kind)))
	}
	b.normalize()
	t.normalize()
	o := &Block{Kind: b.Kind}
	o.normalize()
	for _, n := range b.Order {
		o.Exprs[n] = b.Exprs[n]
		o.Order = append(o.Order, n)
	}
	for _, n := range t.Order {
		w := t.Exprs[n]
		v, ok := o.Exprs[n]
		if ok {
			o.Exprs[n] = merge(v, w)
		} else {
			o.Exprs[n] = w
			o.Order = append(o.Order, n)
		}
	}
	return o
}

type blockHandler struct {
	value func(vals map[string]interface{}, order []string) interface{}
	vtype reflect.Type
}

func (h *blockHandler) assertValue(k uint, v interface{}) {
	if !reflect.TypeOf(v).AssignableTo(h.vtype) {
		panic(fmt.Sprintf("Unexpected value %#v for kind %s", v, lex.SerializeKind(k)))
	}
}

// Takes a kind and a value. Panics if the given value is not of the type which corresponds to the given kind.
func AssertValueForKind(k uint, v interface{}) {
	blockHandlers[k].assertValue(k, v)
}

var blockHandlers map[uint]*blockHandler = make(map[uint]*blockHandler)

// Returns a complex value built from parameters in the receiving block's Exprs and Order. The type of the return value is determined by the receiving block's Kind.
func (b *Block) Eval() interface{} {
	vals, order := b.vals()
	h := blockHandlers[b.Kind]
	out := h.value(vals, order)
	h.assertValue(b.Kind, out)
	return out
}

// Simply returns the receiving block's Kind value.
func (b *Block) ReturnKind() uint {
	return b.Kind
}
