package expr

import (
	"fmt"
	"reflect"

	"gitlab.com/AntiMS/codecomic/dsl/lex"
	"gitlab.com/AntiMS/codecomic/point"
	"gitlab.com/AntiMS/codecomic/pose"
)

func init() {
	blockHandlers[lex.POSE] = &blockHandler{
		value: func(vals map[string]interface{}, order []string) interface{} {
			out := pose.New()
			for _, n := range order {
				v := vals[n]
				switch tv := v.(type) {
				case *pose.Pose:
					out.Merge(tv)
				case *point.PolarPoint:
					out.Add(n, tv)
				default:
					panic(fmt.Sprintf("Parameter %#v value %#v in pose is not a polar point", n, v))
				}
			}
			return out
		},
		vtype: reflect.TypeOf(pose.New()),
	}
}
