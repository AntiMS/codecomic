package expr

import (
	"gitlab.com/AntiMS/codecomic/layout"
	"gitlab.com/AntiMS/codecomic/layout/onerow"
)

func init() {
	RegisterLayoutKind("onerow", []string{}, func(parameters map[string]interface{}) layout.Layout {
		return onerow.OneRow
	})
}
