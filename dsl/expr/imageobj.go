package expr

import (
	"fmt"

	"gitlab.com/AntiMS/codecomic/object"
	"gitlab.com/AntiMS/codecomic/object/image"
	"gitlab.com/AntiMS/codecomic/point"
)

func init() {
	objectHandlers["image"] = func(vals map[string]interface{}, order []string) object.Object {
		p, ok := vals["path"]
		if !ok {
			panic("Required parameter \"path\" absent in image object")
		}
		sp, ok := p.(string)
		if !ok {
			panic(fmt.Sprintf("Non-string \"path\" %#v in image object", p))
		}
		delete(vals, "path")

		a, ok := vals["anchor"]
		if !ok {
			a = point.NewParametric(0.5, 0.5)
		}
		pa, ok := a.(*point.ParametricPoint)
		if !ok {
			panic(fmt.Sprintf("Non-parametric-point \"anchor\" %#v in image object", a))
		}
		delete(vals, "anchor")

		f, ok := vals["flop"]
		if !ok {
			f = false
		}
		bf, ok := f.(bool)
		if !ok {
			panic(fmt.Sprintf("Non-boolean \"flop\" %#v in image object", f))
		}
		delete(vals, "flop")

		r, ok := vals["rotscale"]
		if !ok {
			r = point.PolarIdentity
		}
		pr, ok := r.(*point.PolarPoint)
		if !ok {
			panic(fmt.Sprintf("Non-polar-point \"rotscale\" %#v in image object", r))
		}
		delete(vals, "rotscale")

		l, ok := vals["location"]
		if !ok {
			l = point.ParametricIdentity
		}
		pl, ok := l.(*point.ParametricPoint)
		if !ok {
			panic(fmt.Sprintf("Non-parametric-point \"location\" %#v in image object", l))
		}
		delete(vals, "location")

		if len(vals) != 0 {
			vs := ""
			first := true
			for n, _ := range vals {
				if !first {
					vs += ", "
					first = false
				}
				vs += fmt.Sprintf("%#v", n)
			}
			panic(fmt.Sprintf("Unrecognized parameters in image object: %s", vs))
		}

		return image.NewByPath(sp, pa, bf, pr, pl)
	}
}
