package expr

import (
	"fmt"
	"image"
	"reflect"

	"gitlab.com/AntiMS/codecomic/dsl/lex"
	"gitlab.com/AntiMS/codecomic/frame"
	"gitlab.com/AntiMS/codecomic/layout"
)

type LayoutInfo struct {
	ImageBounds image.Rectangle
	Frames      []frame.Frame
}

type layoutKind struct {
	parameters []string
	construct  func(parameters map[string]interface{}) layout.Layout
}

var kinds map[string]*layoutKind = make(map[string]*layoutKind)

// Registers to a registry a specific kind of layout. The name is the name of the kind (such as "rows"). The parameters slice is a list of names of parameters accepted by this particular kind of layout. The function takes a map of parameters and constructs and returns a layout.Layout from them.
func RegisterLayoutKind(name string, parameters []string, construct func(parameters map[string]interface{}) layout.Layout) {
	kinds[name] = &layoutKind{parameters: parameters, construct: construct}
}

func init() {
	blockHandlers[lex.LAYOUT] = &blockHandler{
		value: func(vals map[string]interface{}, order []string) interface{} {
			tv := vals
			vals = make(map[string]interface{})
			for k, v := range tv {
				vals[k] = v
			}
			to := order
			order = make([]string, 0, len(to))
			for _, v := range to {
				order = append(order, v)
			}

			k, ok := vals["kind"]
			if !ok {
				k = "rows"
			}
			sk, ok := k.(string)
			if !ok {
				panic(fmt.Sprintf("Non-string kind %#v in frame", k))
			}
			lk, ok := kinds[sk]
			if !ok {
				panic(fmt.Sprintf("Layout kind \"%s\" unsupported", sk))
			}

			delete(vals, "kind")
			for i := 0; i < len(order); i++ {
				if order[i] == "kind" {
					order = append(order[:i], order[i+1:]...)
					break
				}
			}

			lkp := make(map[string]interface{})
			for _, n := range lk.parameters {
				v, ok := vals[n]
				if ok {
					lkp[n] = v
					delete(vals, n)
					for i := 0; i < len(order); i++ {
						if order[i] == n {
							order = append(order[:i], order[i+1:]...)
							break
						}
					}
				}
			}

			lay := lk.construct(lkp)

			framePres := make([]*FramePre, 0)
			sizes := make([]struct{ X, Y int }, 0)
			for _, n := range order {
				f, ok := vals[n].(*FramePre)
				if !ok {
					panic(fmt.Sprintf("Non-frame parameter \"%s\" of layout %#v", n, vals[n]))
				}
				framePres = append(framePres, f)
				sizes = append(sizes, f.Size)
			}

			imageBounds, frameBounds := lay.ImageInfo(sizes...)

			frames := make([]frame.Frame, 0)
			for i := 0; i < len(frameBounds) && i < len(framePres); i++ {
				frames = append(frames, frame.New(frameBounds[i], framePres[i].Background, framePres[i].Border, framePres[i].Objects...))
			}

			return &LayoutInfo{ImageBounds: imageBounds, Frames: frames}
		},
		vtype: reflect.TypeOf(&LayoutInfo{}),
	}
}
