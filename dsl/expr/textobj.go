package expr

import (
	"fmt"
	"image/color"

	"gitlab.com/AntiMS/codecomic/object"
	"gitlab.com/AntiMS/codecomic/object/text"
	"gitlab.com/AntiMS/codecomic/point"
)

func init() {
	objectHandlers["text"] = func(vals map[string]interface{}, order []string) object.Object {
		t, ok := vals["text"]
		if !ok {
			panic("Required parameter \"text\" absent in text object")
		}
		st, ok := t.(string)
		if !ok {
			panic(fmt.Sprintf("Non-string \"text\" %#v in text object", t))
		}
		delete(vals, "text")

		c, ok := vals["col"]
		if !ok {
			c = &color.RGBA{R: 0, G: 0, B: 0, A: 255}
		}
		cc, ok := c.(color.Color)
		if !ok {
			panic(fmt.Sprintf("Non-color \"col\" %#v in text object", c))
		}
		delete(vals, "col")

		r, ok := vals["rotscale"]
		if !ok {
			r = point.PolarIdentity
		}
		pr, ok := r.(*point.PolarPoint)
		if !ok {
			panic(fmt.Sprintf("Non-polar-point \"rotscale\" %#v in text object", r))
		}
		delete(vals, "rotscale")

		l, ok := vals["location"]
		if !ok {
			l = point.ParametricIdentity
		}
		pl, ok := l.(*point.ParametricPoint)
		if !ok {
			panic(fmt.Sprintf("Non-parametric-point \"location\" %#v in text object", l))
		}
		delete(vals, "location")

		if len(vals) != 0 {
			vs := ""
			first := true
			for n, _ := range vals {
				if !first {
					vs += ", "
					first = false
				}
				vs += fmt.Sprintf("%#v", n)
			}
			panic(fmt.Sprintf("Unrecognized parameters in text object: %s", vs))
		}

		return text.New(st, cc, pr, pl)
	}
}
