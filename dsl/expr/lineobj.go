package expr

import (
	"fmt"
	"image/color"

	"gitlab.com/AntiMS/codecomic/object"
	"gitlab.com/AntiMS/codecomic/object/line"
	"gitlab.com/AntiMS/codecomic/point"
)

func init() {
	objectHandlers["line"] = func(vals map[string]interface{}, order []string) object.Object {
		t, ok := vals["p1"]
		if !ok {
			panic("Required parameter \"p1\" absent in line object")
		}
		p1, ok := t.(*point.ParametricPoint)
		if !ok {
			panic(fmt.Sprintf("Non-parametric-point \"p1\" %#v in line object", t))
		}
		delete(vals, "p1")

		t, ok = vals["p2"]
		if !ok {
			panic("Required parameter \"p2\" absent in line object")
		}
		p2, ok := t.(*point.ParametricPoint)
		if !ok {
			panic(fmt.Sprintf("Non-parametric-point \"p2\" %#v in line object", t))
		}
		delete(vals, "p2")

		c, ok := vals["col"]
		if !ok {
			c = &color.RGBA{R: 0, G: 0, B: 0, A: 255}
		}
		cc, ok := c.(color.Color)
		if !ok {
			panic(fmt.Sprintf("Non-color \"col\" %#v in line object", c))
		}
		delete(vals, "col")

		if len(vals) != 0 {
			vs := ""
			first := true
			for n, _ := range vals {
				if !first {
					vs += ", "
					first = false
				}
				vs += fmt.Sprintf("%#v", n)
			}
			panic(fmt.Sprintf("Unrecognized parameters in line object: %s", vs))
		}

		return &line.Line{P1: p1, P2: p2, Color: cc}
	}
}
