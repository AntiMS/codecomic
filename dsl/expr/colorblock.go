package expr

import (
	"fmt"
	"image/color"
	"reflect"

	"gitlab.com/AntiMS/codecomic/dsl/lex"
)

func init() {
	getInt := func(vals map[string]interface{}, n string) uint8 {
		v, ok := vals[n]
		if !ok {
			return 0
		}
		fv, ok := v.(float64)
		if !ok {
			panic(fmt.Sprintf("Color parameter %s value %#v is not a number", n, v))
		}
		delete(vals, n)
		return uint8(fv)
	}

	blockHandlers[lex.COLOR] = &blockHandler{
		value: func(vals map[string]interface{}, order []string) interface{} {
			r := getInt(vals, "r")
			g := getInt(vals, "g")
			b := getInt(vals, "b")
			var a uint8
			if _, ok := vals["a"]; !ok {
				a = uint8(255)
			} else {
				a = getInt(vals, "a")
			}

			if len(vals) > 0 {
				panic("Point has parameters with invalid names")
			}

			return &color.RGBA{R: r, G: g, B: b, A: a}
		},
		vtype: reflect.TypeOf((*color.Color)(nil)).Elem(),
	}
}
