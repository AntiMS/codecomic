package expr

// Represents an expression in the domain-specific language.
type Expr interface {
	// Evaluates the expression and returns the go-type value to which the expression evaluates.
	Eval() interface{}
}

// A string literal.
type StringLiteral string

func (l StringLiteral) Eval() interface{} {
	return string(l)
}

// A number literal. All numbers in the DSL are float64s under the hood.
type NumberLiteral float64

func (l NumberLiteral) Eval() interface{} {
	return float64(l)
}

// A boolean literal.
type BooleanLiteral bool

func (l BooleanLiteral) Eval() interface{} {
	return bool(l)
}
