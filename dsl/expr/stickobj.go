package expr

import (
	"fmt"
	"image/color"

	"gitlab.com/AntiMS/codecomic/object"
	"gitlab.com/AntiMS/codecomic/object/stick"
	"gitlab.com/AntiMS/codecomic/point"
	"gitlab.com/AntiMS/codecomic/pose"
	"gitlab.com/AntiMS/codecomic/skeleton"
)

func init() {
	objectHandlers["stick"] = func(vals map[string]interface{}, order []string) object.Object {
		skel, ok := vals["skel"]
		if !ok {
			panic("Required parameter \"skel\" absent in stick object")
		}
		sskel, ok := skel.(*skeleton.Skeleton)
		if !ok {
			panic(fmt.Sprintf("Non-skeleton \"skel\" %#v in stick object", skel))
		}
		delete(vals, "skel")

		stroke, ok := vals["stroke"]
		if !ok {
			stroke = &color.RGBA{R: 0, G: 0, B: 0, A: 255}
		}
		cstroke, ok := stroke.(color.Color)
		if !ok {
			panic(fmt.Sprintf("Non-color \"stroke\" %#v in stick object", stroke))
		}
		delete(vals, "stroke")

		fill, ok := vals["fill"]
		if !ok {
			fill = &color.RGBA{R: 255, G: 255, B: 255, A: 255}
		}
		cfill, ok := fill.(color.Color)
		if !ok {
			panic(fmt.Sprintf("Non-color \"fill\" %#v in stick object", fill))
		}
		delete(vals, "fill")

		flop, ok := vals["flop"]
		if !ok {
			flop = false
		}
		bflop, ok := flop.(bool)
		if !ok {
			panic(fmt.Sprintf("Non-boolean \"flop\" %#v in stick object", flop))
		}
		delete(vals, "flop")

		addons, ok := vals["addons"]
		if !ok {
			addons = nil
		}
		laddons, ok := addons.([]interface{})
		if addons != nil && !ok {
			panic(fmt.Sprintf("Non-list \"addons\" %#v in stick object", addons))
		}
		delete(vals, "addons")
		faddons := make([]stick.Addon, 0)
		for _, aol := range laddons {
			ao, ok := aol.([]interface{})
			if !ok {
				panic(fmt.Sprintf("Non-list item %#v in \"addons\" in stick object", addons))
			}
			if len(ao) != 2 {
				panic(fmt.Sprintf("Item in \"addons\" list in stick object has length %d rather than 2", len(ao)))
			}
			n, ok := ao[0].(string)
			if !ok {
				panic(fmt.Sprintf("Non-string first item %#v in \"addons\" list item in stick object", ao[0]))
			}
			o, ok := ao[1].(object.ModObject)
			if !ok {
				panic(fmt.Sprintf("Non-object or non-modifiable object second item %#v in \"addons\" list item in stick object", ao[1]))
			}
			faddons = append(faddons, stick.Addon{BoneName: n, Obj: o})
		}

		poses, ok := vals["poses"]
		if !ok {
			poses = make([]interface{}, 0)
		}
		lposes, ok := poses.([]interface{})
		if !ok {
			panic(fmt.Sprintf("Non-list \"poses\" %#v in stick object", poses))
		}
		delete(vals, "poses")
		fposes := make([]*pose.Pose, 0)
		for _, p := range lposes {
			pp, ok := p.(*pose.Pose)
			if !ok {
				panic(fmt.Sprintf("Non-pose item %#v in \"poses\" list in stick object", p))
			}
			fposes = append(fposes, pp)
		}

		rotscale, ok := vals["rotscale"]
		if !ok {
			rotscale = point.PolarIdentity
		}
		protscale, ok := rotscale.(*point.PolarPoint)
		if !ok {
			panic(fmt.Sprintf("Non-polar-point \"rotscale\" %#v in stick object", rotscale))
		}
		delete(vals, "rotscale")

		location, ok := vals["location"]
		if !ok {
			location = point.ParametricIdentity
		}
		plocation, ok := location.(*point.ParametricPoint)
		if !ok {
			panic(fmt.Sprintf("Non-parametric-point \"location\" %#v in stick object", location))
		}
		delete(vals, "location")

		if len(vals) != 0 {
			vs := ""
			first := true
			for n, _ := range vals {
				if !first {
					vs += ", "
					first = false
				}
				vs += fmt.Sprintf("%#v", n)
			}
			panic(fmt.Sprintf("Unrecognized parameters in stick object: %s", vs))
		}

		return stick.New(sskel, cstroke, cfill, bflop, faddons, fposes, protscale, plocation)
	}
}
