package expr

import (
	"fmt"
	"reflect"

	"gitlab.com/AntiMS/codecomic/dsl/lex"
	"gitlab.com/AntiMS/codecomic/skeleton"
)

func init() {
	blockHandlers[lex.SKELETON] = &blockHandler{
		value: func(vals map[string]interface{}, order []string) interface{} {
			out := skeleton.New()
			for _, n := range order {
				v := vals[n]
				switch tv := v.(type) {
				case *skeleton.Skeleton:
					out.Merge(tv)
				case *Bone:
					if tv.From == nil {
						out.BoneFromRoot(n, tv.To)
					} else {
						out.BoneFromEnd(n, *(tv.From), tv.To)
					}
				default:
					panic(fmt.Sprintf("Parameter %#v value %#v in skeleton not a skeleton or bone", n, v))
				}
			}
			return out
		},
		vtype: reflect.TypeOf(skeleton.New()),
	}
}
