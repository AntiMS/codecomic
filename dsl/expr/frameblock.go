package expr

import (
	"fmt"
	"image/color"
	"reflect"

	"gitlab.com/AntiMS/codecomic/dsl/lex"
	"gitlab.com/AntiMS/codecomic/object"
	"gitlab.com/AntiMS/codecomic/point"
)

type FramePre struct {
	Size       struct{ X, Y int }
	Background color.Color
	Border     color.Color
	Objects    []object.Object
}

func init() {
	blockHandlers[lex.FRAME] = &blockHandler{
		value: func(vals map[string]interface{}, order []string) interface{} {
			s, ok := vals["size"]
			if !ok {
				s = point.NewParametric(200, 200)
			}
			ps, ok := s.(*point.ParametricPoint)
			if !ok {
				panic(fmt.Sprintf("Size value %#v in frame is not a parametric point", s))
			}
			ss := struct{ X, Y int }{X: int(ps.X()), Y: int(ps.Y())}

			bg, ok := vals["background"]
			if !ok {
				bg = &color.RGBA{R: 255, G: 255, B: 255, A: 255}
			}
			cbg, ok := bg.(color.Color)
			if !ok {
				panic(fmt.Sprintf("Background value %#v in frame is not a color", bg))
			}

			bo, ok := vals["border"]
			if !ok {
				bo = &color.RGBA{R: 0, G: 0, B: 0, A: 255}
			}
			cbo, ok := bo.(color.Color)
			if !ok {
				panic(fmt.Sprintf("Border value %#v in frame is not a color", bo))
			}

			objects := make([]object.Object, 0)
			for _, n := range order {
				if n == "background" || n == "border" || n == "size" {
					continue
				}
				o, ok := vals[n].(object.Object)
				if !ok {
					panic(fmt.Sprintf("Non-object parameter \"%s\" of frame %#v", n, vals[n]))
				}
				objects = append(objects, o)
			}

			return &FramePre{Size: ss, Background: cbg, Border: cbo, Objects: objects}
		},
		vtype: reflect.TypeOf(&FramePre{}),
	}
}
