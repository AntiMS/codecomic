package frame

import (
	"image"
	"image/color"
	"image/draw"

	"github.com/fogleman/gg"

	"gitlab.com/AntiMS/codecomic/object"
	"gitlab.com/AntiMS/codecomic/rectangle"
)

const borderWidth = 2

var margin float64 = 0.05

// Represents one panel in a webcomic. One rectangular portion of the whole image.
type Frame interface {
	// The rectangle in the image to which this frame is to be drawn.
	Bounds() image.Rectangle

	// Draws the frame on the given image.
	Draw(i *image.RGBA)
}

type frame struct {
	rect    image.Rectangle
	objects []object.Object
	bg      color.Color
	border  color.Color
}

// Constructs a frame with the given size, background and border color, and a set of objects to draw in the frame.
func New(rect image.Rectangle, bg color.Color, border color.Color, os ...object.Object) Frame {
	if bg == nil {
		bg = color.White
	}
	if border == nil {
		border = color.Black
	}
	oc := make([]object.Object, len(os))
	copy(oc, os)
	return &frame{
		rect:    rect,
		objects: oc,
		bg:      bg,
		border:  border,
	}
}

func (f *frame) Bounds() image.Rectangle {
	return f.rect
}

func (f *frame) Draw(i *image.RGBA) {
	rects := make([]rectangle.Rectangle, 0, len(f.objects))

	for _, obj := range f.objects {
		rects = append(rects, obj.Bounds())
	}

	bounds := rectangle.ContainingRects(rects...).SmallestEnclosingRect(float64(f.rect.Dx()) / float64(f.rect.Dy())).Scale(1 + margin)

	scale := float64(f.rect.Dx()) / bounds.Width()
	offset := image.Pt(int(-bounds.Min().X()*scale)+f.rect.Min.X, int(bounds.Max().Y()*scale)+f.rect.Min.Y).Sub(f.rect.Min)

	ti := image.NewRGBA(image.Rect(0, 0, f.rect.Dx(), f.rect.Dy()))

	c := gg.NewContextForRGBA(ti)
	c.SetColor(f.bg)
	c.Clear()

	for _, obj := range f.objects {
		obj.Draw(ti, offset, scale)
	}

	c.SetLineWidth(borderWidth * 2)
	c.SetColor(f.border)
	x, y := float64(f.rect.Dx()), float64(f.rect.Dy())
	c.DrawLine(0, 0, x, 0)
	c.DrawLine(0, 0, 0, y)
	c.DrawLine(x, y, x, 0)
	c.DrawLine(x, y, 0, y)
	c.Stroke()
	draw.Draw(i, f.rect, ti, image.ZP, draw.Over)
}
