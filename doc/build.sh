#!/bin/bash

doc_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd "$(dirname "${doc_dir}")"

gopath="$(head -n 1 < './go.mod' | cut '-d ' -f 2)"

ls "${doc_dir}"'/src' | sort -n | while read l ; do
	bn="$(basename "${l}" .comic)"
	go run "${gopath}"'/main' "${doc_dir}"'/src/'"${l}" "${doc_dir}"'/img/'"${bn}"'.png'
done
