package point

import (
	"fmt"
	"math"
	"testing"
)

func areClose(a, e float64) bool {
	return math.Abs(a-e) < 0.00000001
}

func comparePoint(a, e Point) bool {
	switch ta := a.(type) {
	case *ParametricPoint:
		te, ok := e.(*ParametricPoint)
		if !ok {
			return false
		}
		return areClose(ta.X(), te.X()) && areClose(ta.Y(), te.Y())
	case *PolarPoint:
		te, ok := e.(*PolarPoint)
		if !ok {
			return false
		}
		return areClose(ta.R(), te.R()) && areClose(ta.Theta(), te.Theta())
	}
	panic(fmt.Sprintf("Unrecognized point type %#v", a))
}

func assertPoint(t *testing.T, a, e Point) {
	if !comparePoint(a, e) {
		t.Errorf("Comparing points: Expected %s, got %s", e.String(), a.String())
	}
}

func assertString(t *testing.T, p Point, e string) {
	if p.String() != e {
		t.Errorf("Expected point %#v to serialize to %#v but got %#v", p, e, p.String())
	}
}

func TestAll(t *testing.T) {
	assertPoint(t, NewParametric(10, -8).Neg(), NewParametric(-10, 8))
	assertPoint(t, NewParametric(5, -3).Add(NewParametric(19, 2)), NewParametric(24, -1))
	assertPoint(t, NewParametric(-1, 7).ToParametric(), NewParametric(-1, 7))
	assertPoint(t, NewParametric(4, -4).ToPolar(), NewPolar(4*math.Sqrt2, 225))

	assertPoint(t, NewPolar(10, 45).Neg(), NewPolar(0.1, 315))
	assertPoint(t, NewPolar(5, -90).Add(NewPolar(0.4, 135)), NewPolar(2, 45))
	assertPoint(t, NewPolar(8*math.Sqrt2, 135).ToParametric(), NewParametric(-8, -8))
	assertPoint(t, NewPolar(5, 180).ToPolar(), NewPolar(5, 180))

	assertPoint(t, NewParametric(2, 2).Add(NewPolar(2, 90)), NewParametric(0, 2))
	assertPoint(t, NewPolar(2, 90).Add(NewParametric(2, 2)), NewPolar(2*2*math.Sqrt2, 45))

	assertString(t, NewParametric(1.25, 3.75), "(1.250, 3.750)")
	assertString(t, NewPolar(3.125, 225), "(3.125 @ 225.000)")
}
