package point

import (
	"fmt"
	"math"
)

const DEGCONV = math.Pi / 180

// Represents a 2d vector. See the *ParametricPoint and *PolarPoint comments for details.
type Point interface {
	Add(p Point) Point
	Neg() Point
	ToParametric() *ParametricPoint
	ToPolar() *PolarPoint
	String() string
}

// A two-dimensional vector represented parametrically.
type ParametricPoint struct {
	x, y  float64
	polar *PolarPoint
}

// Constructs a new *ParametricPoint given x and y coordinates.
func NewParametric(x, y float64) *ParametricPoint {
	return &ParametricPoint{x: x, y: y}
}

// Returns the vector sum of the receiver point and the (parametric conversion of the) given point.
func (p *ParametricPoint) Add(o Point) Point {
	po := o.ToParametric()
	return NewParametric(p.x+po.x, p.y+po.y)
}

// Returns the additive complement vector as a point.
func (p *ParametricPoint) Neg() Point {
	return NewParametric(-p.x, -p.y)
}

// Returns the receiver point.
func (p *ParametricPoint) ToParametric() *ParametricPoint {
	return p
}

// Converts the receiver point to a *PolarPoint.
func (p *ParametricPoint) ToPolar() *PolarPoint {
	if p.polar == nil {
		var theta float64
		if p.y == 0 || p.y == math.Copysign(0, -1) { // This -0 hack from https://stackoverflow.com/questions/13804255/negative-zero-literal-in-golang
			theta = -90
			if p.x < 0 {
				theta += 180
			}
		} else {
			theta = math.Atan(-p.x/p.y) / DEGCONV
			if p.y < 0 {
				theta += 180
			}
		}
		p.polar = NewPolar(math.Sqrt(p.x*p.x+p.y*p.y), theta)
	}
	return p.polar
}

// Returns a human-readable string representation of this point's x and y coordinates in the form "(1.234, 5.678)".
func (p *ParametricPoint) String() string {
	return fmt.Sprintf("(%.3f, %.3f)", p.x, p.y)
	//return fmt.Sprintf("(%.15f, %.15f)", p.x, p.y)
}

// Returns the x coordinate.
func (p *ParametricPoint) X() float64 {
	return p.x
}

// Returns the y coordinate.
func (p *ParametricPoint) Y() float64 {
	return p.y
}

// A two-dimensional vector represented in polar coordinates.
type PolarPoint struct {
	r, theta   float64
	parametric *ParametricPoint
}

// Constructs a new *PolarPoint given r and theta parameters.
func NewPolar(r, theta float64) *PolarPoint {
	out := &PolarPoint{r: r, theta: theta}
	out.normalize()
	return out
}

// Converts the receiver point (destructively) to a normalized, isomorphic form such that r is non-negative and 0 <= theta < 360.
func (p *PolarPoint) normalize() {
	if p.r < 0 {
		p.r = -p.r
		p.r += 180
	}
	for p.theta < 0 {
		p.theta += 360
	}
	for p.theta >= 360 {
		p.theta -= 360
	}
}

// Despite the name, multiplies the receiver point by the (polar conversion of the) given point.
func (p *PolarPoint) Add(o Point) Point {
	po := o.ToPolar()
	out := NewPolar(p.r*po.r, p.theta+po.theta)
	out.normalize()
	return out
}

// Returns the multiplicative complement vector as a point.
func (p *PolarPoint) Neg() Point {
	p.normalize()
	out := NewPolar(1/p.r, -p.theta)
	out.normalize()
	return out
}

// Converts the receiver point to a *ParametricPoint.
func (p *PolarPoint) ToParametric() *ParametricPoint {
	if p.parametric == nil {
		p.parametric = NewParametric(-math.Sin(p.theta*DEGCONV)*p.r, math.Cos(p.theta*DEGCONV)*p.r)
	}
	return p.parametric
}

// Returns the receiver point.
func (p *PolarPoint) ToPolar() *PolarPoint {
	return p
}

// Returns a human-readable string representation of this point's r and theta parameters in the form "(1.234 @ 5.678)".
func (p *PolarPoint) String() string {
	return fmt.Sprintf("(%.3f @ %.3f)", p.r, p.theta)
}

// Returns the r parameter.
func (p *PolarPoint) R() float64 {
	return p.r
}

// Returns the theta parameter.
func (p *PolarPoint) Theta() float64 {
	return p.theta
}

// The multiplicative identity vector as a *PolarPoint
var PolarIdentity = NewPolar(1, 0)

// The additive identity vector as a *ParametricPoint
var ParametricIdentity = NewParametric(0, 0)
