package image

import (
	"fmt"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"

	"github.com/fogleman/gg"

	"gitlab.com/AntiMS/codecomic/object"
	"gitlab.com/AntiMS/codecomic/point"
	"gitlab.com/AntiMS/codecomic/rectangle"
	"gitlab.com/AntiMS/codecomic/resources"
)

var imageCache map[string]image.Image = make(map[string]image.Image)

type imageObj struct {
	img      image.Image
	anchor   *point.ParametricPoint
	flop     bool
	rotscale *point.PolarPoint
	location *point.ParametricPoint
}

// Creates a new image-type object.
// anchor is the point on the image about which to rotate/scale in percentage of width/height respectively. Nil defaults to 0.5,0.5 .
// if rotscale == point.PolarIdentity, scale is avg(height, width) pixels per unit.
func New(img image.Image, anchor *point.ParametricPoint, flop bool, rotscale *point.PolarPoint, location *point.ParametricPoint) *imageObj {
	if anchor == nil {
		anchor = point.NewParametric(0.5, 0.5)
	}
	if rotscale == nil {
		rotscale = point.PolarIdentity
	}
	if location == nil {
		location = point.ParametricIdentity
	}

	return &imageObj{
		img:      img,
		anchor:   anchor,
		flop:     flop,
		rotscale: rotscale,
		location: location,
	}
}

// Creates an image-type object, pulling the image data from the default ResourceSource. The image data can be in gif, jpeg, png, or any other format for which a codec for go's standard library "image" package is loaded.
func NewByPath(path string, anchor *point.ParametricPoint, flop bool, rotscale *point.PolarPoint, location *point.ParametricPoint) *imageObj {
	if _, ok := imageCache[path]; !ok {
		f, ok := resources.Get(path)
		if !ok {
			panic(fmt.Sprintf("No such image %#v", path))
		}
		defer f.Close()

		img, _, err := image.Decode(f)
		if err != nil {
			panic(err)
		}

		imageCache[path] = img
	}

	return New(imageCache[path], anchor, flop, rotscale, location)
}

func (i *imageObj) canvasScale() float64 {
	bs := i.img.Bounds()
	return i.rotscale.R() * 2 / float64(bs.Dx()+bs.Dy())
}

func (i *imageObj) Bounds() rectangle.Rectangle {
	dx, dy := float64(i.img.Bounds().Dx()), float64(i.img.Bounds().Dy())
	t := point.NewParametric(i.anchor.X()*dx, dy-i.anchor.Y()*dy)
	rs := point.NewPolar(i.canvasScale(), i.rotscale.Theta())
	ps := []*point.ParametricPoint{
		point.NewParametric(0, 0),
		point.NewParametric(dx, 0),
		point.NewParametric(0, dy),
		point.NewParametric(dx, dy),
	}
	for j, _ := range ps {
		ps[j] = i.location.Add(rs.Add(t.Neg().Add(ps[j]))).ToParametric()
	}
	return rectangle.ContainingPoints(ps...)
}

func (i *imageObj) Draw(dst *image.RGBA, offset image.Point, scale float64) {
	//print(i.rotscale.R())
	//print("\n")
	fs := i.canvasScale() * scale
	c := gg.NewContextForRGBA(dst)
	c.Translate(float64(offset.X)+i.location.X()*scale, float64(offset.Y)-i.location.Y()*scale)
	c.Rotate(-gg.Radians(i.rotscale.Theta()))
	c.Scale(fs, fs)
	if i.flop {
		c.Scale(-1, 1)
	}
	c.DrawImageAnchored(i.img, 0, 0, i.anchor.X(), i.anchor.Y())
}

func (i *imageObj) Transform(rotscale *point.PolarPoint, translate *point.ParametricPoint, flop bool) object.ModObject {
	flop = i.flop != flop
	return &imageObj{
		img:      i.img,
		anchor:   i.anchor,
		flop:     flop,
		rotscale: rotscale.Add(i.rotscale).ToPolar(),
		location: translate.Add(i.location).ToParametric(),
	}
}
