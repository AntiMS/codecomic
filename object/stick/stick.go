package stick

import (
	"image"
	"image/color"
	"math"
	"strings"

	"github.com/fogleman/gg"

	"gitlab.com/AntiMS/codecomic/object"
	"gitlab.com/AntiMS/codecomic/point"
	"gitlab.com/AntiMS/codecomic/pose"
	"gitlab.com/AntiMS/codecomic/rectangle"
	"gitlab.com/AntiMS/codecomic/skeleton"
)

const LINE_WIDTH_RATIO = 10

// An item to be "attached" to a stick figure at the end of the indicated bone.
type Addon struct {
	// The bone to which to attach this addon.
	BoneName string

	// The object.ModObject to attach.
	Obj object.ModObject
}

type stick struct {
	segments     skeleton.Segments
	stroke, fill color.Color
	bounds       rectangle.Rectangle
	addons       []Addon
	flop         bool
	rotscale     *point.PolarPoint
	location     *point.ParametricPoint
}

// Returns a new stick-type object which satisfies the object.ModObject interface. Draws a stick figure on the frame.
// flop == true means skeleton should be facing left.
func New(skel *skeleton.Skeleton, stroke, fill color.Color, flop bool, addons []Addon, poses []*pose.Pose, rotscale *point.PolarPoint, location *point.ParametricPoint) *stick {
	if rotscale == nil {
		rotscale = point.PolarIdentity
	}
	if location == nil {
		location = point.ParametricIdentity
	}
	if stroke == nil {
		stroke = color.Black
	}
	if fill == nil {
		fill = color.White
	}

	if poses != nil {
		skel = skel.ApplyPoses(poses...)
	}
	segs := skel.Segments()
	if addons == nil {
		addons = make([]Addon, 0)
	}
	return &stick{
		segments: segs,
		stroke:   stroke,
		fill:     fill,
		addons:   addons,
		flop:     flop,
		rotscale: rotscale,
		location: location,
	}
}

func (s *stick) Bounds() rectangle.Rectangle {
	if s.bounds == nil {
		minx := float64(math.MaxFloat64)
		miny := float64(math.MaxFloat64)
		maxx := float64(-math.MaxFloat64)
		maxy := float64(-math.MaxFloat64)
		sm := make(map[string]*skeleton.Segment)
		segs := s.segments
		if s.flop {
			segs = segs.Flop()
		}
		for _, seg := range segs.Add(s.rotscale).Add(s.location) {
			sm[seg.Name] = seg
			if strings.HasPrefix(seg.Name, "head") {
				cx, cy := (seg.From.X()+seg.To.X())/2, (seg.From.Y()+seg.To.Y())/2
				r := math.Sqrt((seg.From.X()-seg.To.X())*(seg.From.X()-seg.To.X())+(seg.From.Y()-seg.To.Y())*(seg.From.Y()-seg.To.Y())) / 2
				r += float64(0.5) / LINE_WIDTH_RATIO

				minx = math.Min(minx, cx-r)
				miny = math.Min(miny, cy-r)
				maxx = math.Max(maxx, cx+r)
				maxy = math.Max(maxy, cy+r)
			} else if !strings.HasPrefix(seg.Name, "nodraw") {
				b := float64(0.5) / LINE_WIDTH_RATIO

				minx = math.Min(minx, seg.From.X()-b)
				miny = math.Min(miny, seg.From.Y()-b)
				maxx = math.Max(maxx, seg.From.X()+b)
				maxy = math.Max(maxy, seg.From.Y()+b)

				minx = math.Min(minx, seg.To.X()-b)
				miny = math.Min(miny, seg.To.Y()-b)
				maxx = math.Max(maxx, seg.To.X()+b)
				maxy = math.Max(maxy, seg.To.Y()+b)
			}
		}
		for _, a := range s.addons {
			seg := sm[a.BoneName]
			bs := a.Obj.Transform(point.NewPolar(1, seg.Rot), seg.To, s.flop).Bounds()
			if bs.Min().X() < minx {
				minx = bs.Min().X()
			}
			if bs.Min().Y() < miny {
				miny = bs.Min().Y()
			}
			if bs.Max().X() > maxx {
				maxx = bs.Max().X()
			}
			if bs.Max().Y() > maxy {
				maxy = bs.Max().Y()
			}
		}
		s.bounds = rectangle.New(minx, miny, maxx, maxy)
	}
	return s.bounds
}

func (s *stick) Draw(dst *image.RGBA, offset image.Point, scale float64) {
	c := gg.NewContextForRGBA(dst)
	c.SetLineWidth(scale / LINE_WIDTH_RATIO)
	sp := point.NewPolar(scale, 0)
	sm := make(map[string]*skeleton.Segment)
	segs := s.segments
	if s.flop {
		segs = segs.Flop()
	}
	for _, seg := range segs.Add(s.rotscale).Add(s.location) {
		sm[seg.Name] = seg
		seg = seg.Flip().Add(sp)
		if strings.HasPrefix(seg.Name, "head") {
			r := math.Sqrt((seg.From.X()-seg.To.X())*(seg.From.X()-seg.To.X())+(seg.From.Y()-seg.To.Y())*(seg.From.Y()-seg.To.Y())) / 2
			c.DrawCircle((seg.From.X()+seg.To.X())/2+float64(offset.X), (seg.From.Y()+seg.To.Y())/2+float64(offset.Y), r)
			c.SetColor(s.fill)
			c.FillPreserve()
			c.SetColor(s.stroke)
			c.Stroke()
		} else if !strings.HasPrefix(seg.Name, "nodraw") {
			c.SetColor(s.stroke)
			c.DrawLine(seg.From.X()+float64(offset.X), seg.From.Y()+float64(offset.Y), seg.To.X()+float64(offset.X), seg.To.Y()+float64(offset.Y))
			c.Stroke()
		}
	}
	for _, a := range s.addons {
		seg := sm[a.BoneName]
		a.Obj.Transform(point.NewPolar(1, seg.Rot), seg.To, s.flop).Draw(dst, offset, scale)
	}
}

func (s *stick) Transform(rotscale *point.PolarPoint, translate *point.ParametricPoint, flop bool) object.ModObject {
	if rotscale == nil {
		rotscale = point.PolarIdentity
	}
	if translate == nil {
		translate = point.ParametricIdentity
	}
	flop = s.flop != flop
	segs := s.segments
	if flop {
		segs = segs.Flop()
	}
	return &stick{
		segments: segs,
		stroke:   s.stroke,
		fill:     s.fill,
		addons:   s.addons,
		rotscale: rotscale.Add(s.rotscale).ToPolar(),
		location: translate.Add(s.location).ToParametric(),
	}
}
