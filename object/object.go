package object

import (
	"image"

	"gitlab.com/AntiMS/codecomic/point"
	"gitlab.com/AntiMS/codecomic/rectangle"
)

// An item which can be drawn on a frame.
type Object interface {
	// The smallest rectangle which encloses the object to be drawn.
	Bounds() rectangle.Rectangle

	// Draws the object on the given image.
	Draw(dst *image.RGBA, offset image.Point, scale float64) // Scale is in pixels per unit.
}

// An Object which can be scaled, transformed, translated, or flopped.
type ModObject interface {
	Object

	// Returns a copy of this object transformed (rotated, scaled, translated, and/or flopped) as indicated.
	Transform(rotscale *point.PolarPoint, translate *point.ParametricPoint, flop bool) ModObject
}
