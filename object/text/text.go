package text

import (
	"image"
	"image/color"
	"math"
	"strings"

	"github.com/fogleman/gg"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/font/gofont/goregular"

	"gitlab.com/AntiMS/codecomic/object"
	"gitlab.com/AntiMS/codecomic/point"
	"gitlab.com/AntiMS/codecomic/rectangle"
)

var fontFace font.Face

func init() {
	ttf, err := truetype.Parse(goregular.TTF)
	if err != nil {
		panic(err)
	}
	opts := &truetype.Options{}
	opts.DPI = 96
	opts.Size = 72
	fontFace = truetype.NewFace(ttf, opts)
}

type textObj struct {
	lines    int
	img      image.Image
	rotscale *point.PolarPoint
	location *point.ParametricPoint
}

// Returns a new text-type object which satisfies the object.ModObject interface. Draws text on the frame.
func New(text string, col color.Color, rotscale *point.PolarPoint, location *point.ParametricPoint) *textObj {
	if col == nil {
		col = color.Black
	}

	ctx := gg.NewContext(1, 1)
	ctx.SetFontFace(fontFace)
	texts := strings.Split(text, "\n")
	var lineHeight, width float64
	for _, t := range texts {
		w, h := ctx.MeasureString(t)
		if w > width {
			width = w
		}
		if h > lineHeight {
			lineHeight = h
		}
	}
	height := lineHeight * (float64(len(texts)) + 0.4)

	r := image.NewRGBA(image.Rect(0, 0, int(math.Ceil(width)), int(math.Ceil(height))))
	ctx = gg.NewContextForRGBA(r)
	ctx.SetFontFace(fontFace)
	ctx.SetColor(col)
	for i, t := range texts {
		ctx.DrawStringAnchored(t, width/2, lineHeight*(float64(i)+0.5), 0.5, 0.5)
	}

	return &textObj{lines: len(texts), img: r, rotscale: rotscale, location: location}
}

func (t *textObj) Bounds() rectangle.Rectangle {
	w, h := float64(t.img.Bounds().Dx()), float64(t.img.Bounds().Dy())
	w, h = w/h, 1
	w, h = w*float64(t.lines), h*float64(t.lines)
	ps := []*point.ParametricPoint{
		point.NewParametric(-w/2, -h/2),
		point.NewParametric(w/2, -h/2),
		point.NewParametric(-w/2, h/2),
		point.NewParametric(w/2, h/2),
	}
	for j, _ := range ps {
		ps[j] = t.location.Add(t.rotscale.Add(ps[j])).ToParametric()
	}
	return rectangle.ContainingPoints(ps...)
}

func (t *textObj) Draw(dst *image.RGBA, offset image.Point, scale float64) {
	s := scale * t.rotscale.R() * float64(t.lines) / float64(t.img.Bounds().Dy())
	c := gg.NewContextForRGBA(dst)
	c.Translate(float64(offset.X)+t.location.X()*scale, float64(offset.Y)-t.location.Y()*scale)
	c.Rotate(-gg.Radians(t.rotscale.Theta()))
	c.Scale(s, s)
	c.DrawImageAnchored(t.img, 0, 0, 0.5, 0.5)
}

// Flop is ignored for text objects.
func (t *textObj) Transform(rotscale *point.PolarPoint, translate *point.ParametricPoint, _ bool) object.ModObject {
	return &textObj{img: t.img, rotscale: rotscale.Add(t.rotscale).ToPolar(), location: translate.Add(t.location).ToParametric()}
}
