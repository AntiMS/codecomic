package line

import (
	"image"
	"image/color"

	"github.com/fogleman/gg"

	"gitlab.com/AntiMS/codecomic/object"
	"gitlab.com/AntiMS/codecomic/point"
	"gitlab.com/AntiMS/codecomic/rectangle"
)

const LINE_WIDTH_RATIO = 10

// Satisfies the object.ModObject interface. Draws a line from P1 to P2 in the indicated color.
type Line struct {
	P1, P2 *point.ParametricPoint
	Color  color.Color
}

func (l *Line) Bounds() rectangle.Rectangle {
	return rectangle.New(l.P1.X(), l.P1.Y(), l.P2.X(), l.P2.Y()).Grow(float64(0.5) / LINE_WIDTH_RATIO)
}

func (l *Line) Draw(dst *image.RGBA, offset image.Point, scale float64) {
	c := gg.NewContextForRGBA(dst)
	ox, oy := float64(offset.X), float64(offset.Y)
	x1, x2, y1, y2 := l.P1.X(), l.P2.X(), l.P1.Y(), l.P2.Y()
	x1, x2, y1, y2 = ox+x1*scale, ox+x2*scale, oy-y1*scale, oy-y2*scale
	c.SetLineWidth(scale / LINE_WIDTH_RATIO)
	c.SetColor(l.Color)
	c.DrawLine(x1, y1, x2, y2)
	c.Stroke()
}

func (l *Line) Transform(rotscale *point.PolarPoint, translate *point.ParametricPoint, flop bool) object.ModObject {
	p1, p2 := l.P1, l.P2
	if flop {
		p1, p2 = point.NewParametric(p2.X(), p1.Y()), point.NewParametric(p1.X(), p2.Y())
	}
	c := rectangle.New(p1.X(), p1.Y(), p2.X(), p2.Y()).Center()
	return &Line{
		P1:    rotscale.Add(p1.Add(c.Neg())).ToParametric().Add(c).Add(translate).ToParametric(),
		P2:    rotscale.Add(p2.Add(c.Neg())).ToParametric().Add(c).Add(translate).ToParametric(),
		Color: l.Color,
	}
}
