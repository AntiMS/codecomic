package line

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"math"
	"os"
	"testing"

	"gitlab.com/AntiMS/codecomic/point"
	"gitlab.com/AntiMS/codecomic/rectangle"
)

const (
	DIM   = 1000
	SCALE = 300
)

// Setting this environment variable when running this unit test will cause the unit test to write two files -- "/tmp/img1.png" and "/tmp/img2.png" -- useful for debugging this test if it is failing.
var debug bool = os.Getenv("CODECOMIC_UNIT_TEST_DEBUG") == "true"

func areClose(a, e float64) bool {
	return math.Abs(a-e) < 0.00000001
}

func comparePoint(a, e point.Point) bool {
	switch ta := a.(type) {
	case *point.ParametricPoint:
		te, ok := e.(*point.ParametricPoint)
		if !ok {
			return false
		}
		return areClose(ta.X(), te.X()) && areClose(ta.Y(), te.Y())
	case *point.PolarPoint:
		te, ok := e.(*point.PolarPoint)
		if !ok {
			return false
		}
		return areClose(ta.R(), te.R()) && areClose(ta.Theta(), te.Theta())
	}
	panic(fmt.Sprintf("Unrecognized point type %#v", a))
}

func assertPoint(t *testing.T, a, e point.Point) {
	if !comparePoint(a, e) {
		t.Errorf("Comparing points: Expected %s, got %s", e.String(), a.String())
	}
}

func rectToString(r rectangle.Rectangle) string {
	return r.Min().String() + "->" + r.Max().String()
}

func assertRect(t *testing.T, a, e rectangle.Rectangle) {
	if !comparePoint(a.Min(), e.Min()) || !comparePoint(a.Max(), e.Max()) {
		t.Errorf("Comparing rects: Expected %s, got %s", rectToString(e), rectToString(a))
	}
}

// Tests all pixels in the image for transparency or opacity on the basis of whether they should lie inside the line drawn or outside excepting a thin swath of pixels right at the border between those pixels expected to be fully transparent and those expected to be fully opaque.
func assertImage(t *testing.T, img image.Image, col color.Color, scale, x1, y1, x2, y2 float64) {
	// halfMargin is half (hence "/ 2.0") the width (in pixels) of the swath of pixels at the border between opaque (part of the line) and transparent (not part of the line) not to check (since they'll be of varying degrees of transparency).
	// Smaller values are more strict. Larger are more lenient.
	halfMargin := 3.0 / 2.0
	halfWid := scale * 0.05
	min, max := halfWid-halfMargin, halfWid+halfMargin
	for x := 0; x < DIM; x++ {
		for y := 0; y < DIM; y++ {
			fx, fy := float64(x)+0.5, float64(y)+0.5
			tx1, ty1, tx2, ty2 := x1-fx, y1-fy, x2-fx, y2-fy
			d12s := (tx2-tx1)*(tx2-tx1) + (ty2-ty1)*(ty2-ty1)
			d1s := tx1*tx1 + ty1*ty1
			d2s := tx2*tx2 + ty2*ty2
			var d float64
			switch {
			case d1s+d12s < d2s:
				d = math.Sqrt(d1s)
			case d2s+d12s < d1s:
				d = math.Sqrt(d2s)
			default:
				d = math.Abs((tx2-tx1)*ty1-(ty2-ty1)*tx1) / math.Sqrt(d12s)
			}
			c := img.At(x, y)
			if d < min && c != col {
				t.Errorf("Expected point (%d, %d) (d = %.3f) to be opaque green, but was %#v", x, y, d, c)
			} else if _, _, _, a := c.RGBA(); d > max && a != 0 {
				t.Errorf("Expected point (%d, %d) (d = %.3f) to be transparent, but opacity was %d", x, y, d, a)
			}

			if debug {
				if d < min {
					img.(draw.Image).Set(x, y, color.RGBA{0, 0, 255, 255})
				} else if d > max {
					img.(draw.Image).Set(x, y, color.RGBA{255, 0, 0, 255})
				}
			}
		}
	}
}

func TestAll(t *testing.T) {
	col := color.RGBA{R: 0, G: 128, B: 0, A: 255}
	l := &Line{P1: point.NewParametric(1, 1), P2: point.NewParametric(2, 4), Color: col}

	assertRect(t, l.Bounds(), rectangle.New(0.95, 0.95, 2.05, 4.05))

	img := image.NewRGBA(image.Rect(0, 0, DIM, DIM))
	l.Draw(img, image.Pt(int(DIM/2-SCALE*1.5), int(DIM/2+SCALE*2.5)), SCALE)
	assertImage(t, img, col, SCALE, 350.0, 950.0, 650.0, 50.0)

	if debug {
		f, err := os.Create("/tmp/img1.png")
		if err != nil {
			panic(err)
		}
		err = png.Encode(f, img)
		if err != nil {
			panic(err)
		}
		f.Close()
	}

	l = l.Transform(point.NewPolar(2, 22.5), point.NewParametric(1, -4), true).(*Line)

	a := 22.5 * point.DEGCONV
	m := 0.05
	s := 2.0
	tx, ty := 1.0, -4.0
	cx, cy := 1.5, 2.5
	p1x, p1y, p2x, p2y := 1.0-cx, 4.0-cy, 2.0-cx, 1.0-cy
	afs := func(x, y float64) float64 {
		out := math.Atan(y / x)
		if x < 0 {
			out += math.Pi
		}
		return out
	}
	pol1r, pol1t := s*math.Sqrt(p1x*p1x+p1y*p1y), afs(p1x, p1y)+a
	pol2r, pol2t := s*math.Sqrt(p2x*p2x+p2y*p2y), afs(p2x, p2y)+a
	par1x, par1y := pol1r*math.Cos(pol1t)+cx+tx, pol1r*math.Sin(pol1t)+cy+ty
	par2x, par2y := pol2r*math.Cos(pol2t)+cx+tx, pol2r*math.Sin(pol2t)+cy+ty

	assertRect(t, l.Bounds(), rectangle.New(par1x-m, par2y-m, par2x+m, par1y+m))

	cx, cy = (par1x+par2x)/2, (par1y+par2y)/2

	img = image.NewRGBA(image.Rect(0, 0, DIM, DIM))
	l.Draw(img, image.Pt(int(DIM/2-SCALE*cx/2), int(DIM/2+SCALE*cy/2)), SCALE/2)
	assertImage(t, img, col, SCALE/2, DIM/2-SCALE*(par2x-par1x)/4, DIM/2+SCALE*(par2y-par1y)/4, DIM/2+SCALE*(par2x-par1x)/4, DIM/2-SCALE*(par2y-par1y)/4)

	if debug {
		f, err := os.Create("/tmp/img2.png")
		if err != nil {
			panic(err)
		}
		err = png.Encode(f, img)
		if err != nil {
			panic(err)
		}
		f.Close()
	}
}
