package assets

import (
	"bytes"
	"io"
)

type mockCloser struct {
	io.Reader
}

func (c mockCloser) Close() error {
	return nil
}

// Satisfies the resources.ResourceSource interface. Returns resources built into the CodeComic binary.
type AssetsSource struct{}

func (s *AssetsSource) Get(path string) (io.ReadCloser, bool) {
	out, ok := assetsMap[path]
	if !ok {
		return nil, false
	}
	return &mockCloser{Reader: bytes.NewReader(out)}, true
}
