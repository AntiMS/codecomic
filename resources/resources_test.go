package resources

import (
	"io"
	"testing"
)

type mockSource struct {
	success bool
}

func (s *mockSource) Get(path string) (io.ReadCloser, bool) {
	if s.success {
		return s, true
	}
	return nil, false
}

func (s *mockSource) Read(p []byte) (int, error) {
	return 0, nil
}

func (s *mockSource) Close() error {
	return nil
}

func doTest(t *testing.T, ei int, bs ...bool) {
	m := make(Multiplexer, 0, len(bs))

	for _, b := range bs {
		m = append(m, &mockSource{b})
	}

	as, aok := m.Get("")
	var mas *mockSource
	if as != nil {
		mas = as.(*mockSource)
	}

	var ai int = -1
	for i, s := range m {
		if s == mas {
			ai = i
			break
		}
	}

	switch {
	case aok && as == nil:
		t.Errorf("Multiplexer returned nil source with true ok")
	case !aok && as != nil:
		if ai == -1 {
			t.Errorf("Multiplexer returned non-nil source (not given) with false ok")
		} else {
			t.Errorf("Multiplexer returned non-nil source %d with false ok", ai)
		}
	case as != nil && ai == -1:
		t.Errorf("Multiplexer returned non-nil source not given")
	case aok && ei == -1:
		t.Errorf("Multiplexer unexpectedly found source %d", ai)
	case !aok && ei != -1:
		t.Errorf("Multiplexer failed to find expected source %d", ei)
	case ai != ei:
		t.Errorf("Multiplexer returned source %d but expected %d", ai, ei)
	}
}

func TestMultiplexer(t *testing.T) {
	doTest(t, 2, false, false, true, false, true)
	doTest(t, 1, false, true, false, false, false)
	doTest(t, -1, false, false, false, false, false)
	doTest(t, 0, true, true, true, true, true)
	doTest(t, 0, true)
	doTest(t, -1, false)
	doTest(t, -1)
}
