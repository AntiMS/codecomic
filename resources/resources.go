package resources

import (
	"io"
)

// A "respository" of static data resources. Implementations might allow for fetching data from the filesystem or any other particular source.
type ResourceSource interface {
	Get(path string) (io.ReadCloser, bool)
}

// Wraps multiple ResourceSources and also satisfies the ResourceSource interface.
type Multiplexer []ResourceSource

// Checks wrapped ResourceSources in order one-by-one, returning the first success it finds.
func (m Multiplexer) Get(path string) (io.ReadCloser, bool) {
	for _, s := range m {
		out, ok := s.Get(path)
		if ok {
			return out, true
		}
	}
	return nil, false
}

// A default ResourceSource. A Multiplexer. (Unless/until overridden.)
var DefaultSource ResourceSource = make(Multiplexer, 0)

// Appends a ResourceSource to the DefaultSource.
func AddSourcesToDefault(sources ...ResourceSource) {
	DefaultSource = append(Multiplexer(sources), DefaultSource.(Multiplexer))
}

// Gets a resource (io.ReadCloser) from the DefalutSource.
func Get(path string) (io.ReadCloser, bool) {
	return DefaultSource.Get(path)
}
