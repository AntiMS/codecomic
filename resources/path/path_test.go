package path

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func writeFile(t *testing.T, path, name, text string) {
	f, err := os.Create(filepath.Join(path, name))
	if err != nil {
		t.Error(err)
	}
	defer f.Close()

	_, err = f.Write([]byte(text))
	if err != nil {
		t.Error(err)
	}
}

func assertFileContents(t *testing.T, source PathSource, name, contents string) {
	r, ok := source.Get(name)
	if ok {
		if r == nil {
			t.Errorf("PathResource returned nil reader with true ok for %s", name)
		}
	} else {
		if r == nil {
			t.Errorf("PathResource returned non-nil reader with false ok for %s", name)
		} else {
			t.Errorf("File %s does not exist", name)
		}
	}

	b, err := ioutil.ReadAll(r)
	if err != nil {
		t.Error(err)
	}

	s := string(b)
	if contents != s {
		t.Errorf("File %s contents %#v; expected %#v", name, s, contents)
	}
}

func assertFileDoesNotExist(t *testing.T, source PathSource, name string) {
	r, ok := source.Get(name)
	if ok {
		if r == nil {
			t.Errorf("PathResource return nil reader with true ok for %s", name)
		} else {
			t.Errorf("File %s expected not to exist but does", name)
		}
	} else {
		if r != nil {
			t.Errorf("PathResource returned non-nil reader with false ok for %s", name)
		}
	}
}

func TestPath(t *testing.T) {
	// Setup test data
	tempdir, err := os.MkdirTemp("", "codecomic_test_")
	if err != nil {
		t.Error(err)
	}

	writeFile(t, tempdir, "a.txt", "Argyle")
	writeFile(t, tempdir, "b.txt", "Blowfish")
	err = os.Mkdir(filepath.Join(tempdir, "directory"), 0755)
	if err != nil {
		t.Error(err)
	}
	writeFile(t, tempdir, "directory/c.txt", "Culture")

	// Test
	source := PathSource(tempdir)

	assertFileContents(t, source, "a.txt", "Argyle")
	assertFileContents(t, source, "b.txt", "Blowfish")
	assertFileContents(t, source, "directory/c.txt", "Culture")

	assertFileContents(t, source, "a.txt", "Argyle")
	assertFileDoesNotExist(t, source, "c.txt")
	assertFileDoesNotExist(t, source, "d.txt")
	assertFileDoesNotExist(t, source, "a")
	assertFileDoesNotExist(t, source, "A.txt")
	assertFileDoesNotExist(t, source, "directory/a.txt")
	assertFileDoesNotExist(t, source, "directory/b.txt")
	assertFileDoesNotExist(t, source, "directory/c")

	// Cleanup
	err = os.RemoveAll(tempdir)
	if err != nil {
		t.Error(err)
	}
}
