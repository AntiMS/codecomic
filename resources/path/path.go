package path

import (
	"errors"
	"io"
	"os"
	"path/filepath"
)

// Satisfies the resources.ResourceSource interface. Pulls resources from a path on the filesystem.
type PathSource string

func (s PathSource) Get(path string) (io.ReadCloser, bool) {
	fullpath := filepath.Join(string(s), path)
	out, err := os.Open(fullpath)
	if errors.Is(err, os.ErrNotExist) {
		return nil, false
	} else if err != nil {
		panic(err)
	}
	return out, true
}

// A PathSource which pulls from the working directory.
var WorkingDirectorySource PathSource

func init() {
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	WorkingDirectorySource = PathSource(wd)
}
