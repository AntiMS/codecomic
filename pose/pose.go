package pose

import (
	"gitlab.com/AntiMS/codecomic/point"
)

// Represents a "pose", a set of angles per bone used to transform a *Skeleton.
type Pose struct {
	Points map[string]*point.PolarPoint
}

// Adds a new point to this pose to alter the angle of a bone any *Skeleton to which this pose would be applied.
func (p *Pose) Add(n string, pt *point.PolarPoint) {
	c, ok := p.Points[n]
	if ok {
		pt = c.Add(pt).ToPolar()
	}
	p.Points[n] = pt
}

// Copies all of the points in the given pose into this pose.
func (p *Pose) Merge(o *Pose) {
	for n, pt := range o.Points {
		p.Add(n, pt)
	}
}

// Constructs a new empty pose.
func New() *Pose {
	return &Pose{Points: make(map[string]*point.PolarPoint)}
}
