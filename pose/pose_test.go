package pose

import (
	"math"
	"testing"

	"gitlab.com/AntiMS/codecomic/point"
)

func areClose(a, e float64) bool {
	return math.Abs(a-e) < 0.00000001
}

func comparePoint(a, e *point.PolarPoint) bool {
	return areClose(a.R(), e.R()) && areClose(a.Theta(), e.Theta())
}

func assertPoint(t *testing.T, a, e *point.PolarPoint) {
	if !comparePoint(a, e) {
		t.Errorf("Comparing points: Expected %s, got %s", e.String(), a.String())
	}
}

func assertPosePoint(t *testing.T, p *Pose, n string, e *point.PolarPoint) {
	a, ok := p.Points[n]
	if !ok {
		t.Errorf("Expected a pose point named %#v, but no such point was found", n)
	}
	assertPoint(t, a, e)
}

func TestAll(t *testing.T) {
	p1 := New()
	p1.Add("funny", point.NewPolar(2, 5))
	p1.Add("dog", point.NewPolar(1, 90))

	p2 := New()
	p2.Add("funny", point.NewPolar(1, 15))
	p2.Add("trom", point.NewPolar(1, 10))

	p1.Merge(p2)

	if len(p1.Points) != 3 {
		t.Errorf("Merged pose has %d points rather than 3", len(p1.Points))
	}
	assertPosePoint(t, p1, "funny", point.NewPolar(2, 20))
	assertPosePoint(t, p1, "dog", point.NewPolar(1, 90))
	assertPosePoint(t, p1, "trom", point.NewPolar(1, 10))
	for k, _ := range p1.Points {
		if k != "funny" && k != "dog" && k != "trom" {
			t.Errorf("Unexpected point %#v in merged pose", k)
		}
	}
}
