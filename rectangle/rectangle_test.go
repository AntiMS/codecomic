package rectangle

import (
	"fmt"
	"math"
	"testing"

	"gitlab.com/AntiMS/codecomic/point"
)

func rectToString(r Rectangle) string {
	return r.Min().String() + "->" + r.Max().String()
}

func areClose(a, e float64) bool {
	return math.Abs(a-e) < 0.00000001
}

func comparePoint(a, e point.Point) bool {
	switch ta := a.(type) {
	case *point.ParametricPoint:
		te, ok := e.(*point.ParametricPoint)
		if !ok {
			return false
		}
		return areClose(ta.X(), te.X()) && areClose(ta.Y(), te.Y())
	case *point.PolarPoint:
		te, ok := e.(*point.PolarPoint)
		if !ok {
			return false
		}
		return areClose(ta.R(), te.R()) && areClose(ta.Theta(), te.Theta())
	}
	panic(fmt.Sprintf("Unrecognized point type %#v", a))
}

func assertPoint(t *testing.T, a, e point.Point) {
	if !comparePoint(a, e) {
		t.Errorf("Comparing points: Expected %s, got %s", e.String(), a.String())
	}
}

func assertRect(t *testing.T, a, e Rectangle) {
	if !comparePoint(a.Min(), e.Min()) || !comparePoint(a.Max(), e.Max()) {
		t.Errorf("Comparing rects: Expected %s, got %s", rectToString(e), rectToString(a))
	}
}

func TestAll(t *testing.T) {
	assertRect(t, New(2, 2, 1, 1), New(1, 1, 2, 2))
	assertRect(t, ContainingPoints(
		point.NewParametric(-1, -1),
		point.NewParametric(1, 0),
		point.NewParametric(0, 1),
	), New(-1, -1, 1, 1))
	assertRect(t, ContainingRects(
		New(-2, -2, -3, -3),
		New(0, 0, 10, 1),
		New(-1, -4, 1, 2),
	), New(-3, -4, 10, 2))
	assertRect(t, New(-2, -2, 0, 2).SmallestEnclosingRect(1.5), New(-4, -2, 2, 2))
	assertRect(t, New(-2, -2, 0, 2).Grow(2), New(-4, -4, 2, 4))
	assertRect(t, New(-2, -2, 0, 2).Scale(1.5), New(-2.5, -3, 0.5, 3))
	assertRect(t, New(-2, -2, 0, 2).Translate(point.NewParametric(3, 7)), New(1, 5, 3, 9))
	if ar := New(-2, -2, 0, 2).AspectRatio(); !areClose(ar, 0.5) {
		t.Errorf("Expected an aspect ratio of 0.5, but got %f", ar)
	}
	assertPoint(t, New(-2, -2, 0, 2).Center(), point.NewParametric(-1, 0))
	if maxdim := New(-2, -2, 0, 2).MaxDim(); !areClose(maxdim, 4) {
		t.Errorf("Expected a maximum dimension of 4, but got %f", maxdim)
	}
	if mindim := New(-2, -2, 0, 2).MinDim(); !areClose(mindim, 2) {
		t.Errorf("Expected a maximum dimension of 2, but got %f", mindim)
	}
	if width := New(-2, -2, 0, 2).Width(); !areClose(width, 2) {
		t.Errorf("Expected a width of 2, but got %f", width)
	}
	if height := New(-2, -2, 0, 2).Height(); !areClose(height, 4) {
		t.Errorf("Expected a height of 4, but got %f", height)
	}
	assertPoint(t, New(-2, -2, 0, 2).Min(), point.NewParametric(-2, -2))
	assertPoint(t, New(-2, -2, 0, 2).Max(), point.NewParametric(0, 2))
}
