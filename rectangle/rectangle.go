package rectangle

import (
	"math"

	"gitlab.com/AntiMS/codecomic/point"
)

type Rectangle interface {
	// Returns the corner point of this rectangle with minimum x and y coordinates.
	Min() *point.ParametricPoint

	// Returns the corner point of this rectangle with maximum x and y coordinates.
	Max() *point.ParametricPoint

	// Returns the difference of the maximum and minimum x coordinates.
	Width() float64

	// Returns the difference of the maximum and minimum y coordinates.
	Height() float64

	// Returns the maximum dimension.
	MaxDim() float64

	// Returns the minimum dimension.
	MinDim() float64

	// Returns the center point of this rectangle.
	Center() *point.ParametricPoint

	// Returns the width of this rectangle divided by the height.
	AspectRatio() float64

	// Returns a copy of this rectangle translated by the vector p.
	Translate(p *point.ParametricPoint) Rectangle

	// Returns a concentric copy of this rectangle scaled by the given scale.
	Scale(scale float64) Rectangle

	// Returns a Rectangle with max moved in the positive X and positive Y direction by v units, and min moved in the negative X and negative Y direction by the same amount.
	Grow(v float64) Rectangle

	// Returns the smallest concentric Rectangle with the given aspect ratio (width/height) containing the receiving rectangle.
	SmallestEnclosingRect(ar float64) Rectangle
}

type rectangle struct {
	min, max *point.ParametricPoint
}

func (r *rectangle) Min() *point.ParametricPoint {
	return r.min
}
func (r *rectangle) Max() *point.ParametricPoint {
	return r.max
}
func (r *rectangle) Width() float64 {
	return r.max.X() - r.min.X()
}
func (r *rectangle) Height() float64 {
	return r.max.Y() - r.min.Y()
}
func (r *rectangle) MaxDim() float64 {
	return math.Max(r.Width(), r.Height())
}
func (r *rectangle) MinDim() float64 {
	return math.Min(r.Width(), r.Height())
}
func (r *rectangle) Center() *point.ParametricPoint {
	return point.NewParametric((r.min.X()+r.max.X())/2, (r.min.Y()+r.max.Y())/2)
}
func (r *rectangle) AspectRatio() float64 {
	return r.Width() / r.Height()
}
func (r *rectangle) Translate(p *point.ParametricPoint) Rectangle {
	return &rectangle{
		min: p.Add(r.Min()).ToParametric(),
		max: p.Add(r.Max()).ToParametric(),
	}
}
func (r *rectangle) Scale(factor float64) Rectangle {
	c := r.Center()
	s := point.NewPolar(factor, 0)
	min := c.Add(s.Add(r.Min().Add(c.Neg()))).ToParametric()
	max := c.Add(s.Add(r.Max().Add(c.Neg()))).ToParametric()
	return New(min.X(), min.Y(), max.X(), max.Y())
}

func (r *rectangle) Grow(v float64) Rectangle {
	return New(r.Min().X()-v, r.Min().Y()-v, r.Max().X()+v, r.Max().Y()+v)
}

func (r *rectangle) SmallestEnclosingRect(ar float64) Rectangle {
	car := r.AspectRatio()
	if car > ar {
		o := (r.Width()/ar - r.Height()) / 2
		return New(r.Min().X(), r.Min().Y()-o, r.Max().X(), r.Max().Y()+o)
	}
	o := (r.Height()*ar - r.Width()) / 2
	return New(r.Min().X()-o, r.Min().Y(), r.Max().X()+o, r.Max().Y())
}

// Constructs a rectangle given minimum and maximum x and y coordinates.
func New(x1, y1, x2, y2 float64) Rectangle {
	return &rectangle{
		min: point.NewParametric(math.Min(x1, x2), math.Min(y1, y2)),
		max: point.NewParametric(math.Max(x1, x2), math.Max(y1, y2)),
	}
}

// Returns the smallest rectangle containing all given points.
func ContainingPoints(ps ...*point.ParametricPoint) Rectangle {
	minx := math.MaxFloat64
	maxx := -math.MaxFloat64
	miny := math.MaxFloat64
	maxy := -math.MaxFloat64
	for _, p := range ps {
		if p.X() < minx {
			minx = p.X()
		}
		if p.X() > maxx {
			maxx = p.X()
		}
		if p.Y() < miny {
			miny = p.Y()
		}
		if p.Y() > maxy {
			maxy = p.Y()
		}
	}
	if maxx < minx {
		minx = (minx + maxx) / 2
		maxx = minx
	}
	if maxy < miny {
		miny = (miny + maxy) / 2
		maxy = miny
	}
	return New(minx, miny, maxx, maxy)
}

// Returns the smallest rectangle containing all given rectangles.
func ContainingRects(rects ...Rectangle) Rectangle {
	ps := make([]*point.ParametricPoint, 0, len(rects)*2)
	for _, r := range rects {
		ps = append(ps, r.Min(), r.Max())
	}
	return ContainingPoints(ps...)
}
