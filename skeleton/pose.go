package skeleton

import (
	"gitlab.com/AntiMS/codecomic/point"
	"gitlab.com/AntiMS/codecomic/pose"
)

// Returns a copy of this skeleton with the given poses applied. (That is to say, with each bone in the skeleton multiplied by the point by the same name in the pose.)
func (s *Skeleton) ApplyPoses(poses ...*pose.Pose) *Skeleton {
	out := s.copy()
	for _, pose := range poses {
		for n, p := range pose.Points {
			b, ok := out.bones[n]
			if !ok {
				print("WARNING: no such bone " + n + "\n")
				continue
			}
			b.to = b.to.Add(p).(*point.PolarPoint)
		}
	}
	return out
}
