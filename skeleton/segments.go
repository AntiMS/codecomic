package skeleton

import (
	"gitlab.com/AntiMS/codecomic/point"
)

func getAdd(pt point.Point) (func(p *point.ParametricPoint) *point.ParametricPoint, func(r float64) float64) {
	return func(p *point.ParametricPoint) *point.ParametricPoint {
			return pt.Add(p).ToParametric()
		}, func(r float64) float64 {
			pol, ok := pt.(*point.PolarPoint)
			if ok {
				return r + pol.Theta()
			}
			return r
		}
}

func flipPar(p *point.ParametricPoint) *point.ParametricPoint {
	return point.NewParametric(p.X(), -p.Y())
}
func flipPol(r float64) float64 {
	return 180 - r
}

func flopPar(p *point.ParametricPoint) *point.ParametricPoint {
	return point.NewParametric(-p.X(), p.Y())
}
func flopPol(r float64) float64 {
	return -r
}

// A named line segment.
type Segment struct {
	// A name of this segment.
	Name string

	// The origin and destination points of this segment.
	From, To *point.ParametricPoint

	// The angle at which this segment is rotated (relative to the universal "zero" rotation: upward.)
	Rot float64
}

func (s *Segment) transform(t func(p *point.ParametricPoint) *point.ParametricPoint, r func(r float64) float64) *Segment {
	newrot := s.Rot
	if r != nil {
		newrot = r(newrot)
	}
	return &Segment{
		Name: s.Name,
		From: t(s.From),
		To:   t(s.To),
		Rot:  newrot,
	}
}

// Returns a copy of this segment translated by the given point.
func (s *Segment) Add(pt point.Point) *Segment {
	return s.transform(getAdd(pt))
}

// Returns a copy of this segment mirrored about the x axis.
func (s *Segment) Flip() *Segment {
	return s.transform(flipPar, flipPol)
}

// Returns a copy of this segment mirrored about the y axis.
func (s *Segment) Flop() *Segment {
	return s.transform(flopPar, flopPol)
}

// A collection of named line segments.
type Segments []*Segment

func (segs Segments) transform(t func(p *point.ParametricPoint) *point.ParametricPoint, r func(r float64) float64) Segments {
	out := make(Segments, 0, len(segs))
	for _, seg := range segs {
		out = append(out, seg.transform(t, r))
	}
	return out
}

// Returns a copy of this collection of segments with each segment translated by the given point.
func (s Segments) Add(pt point.Point) Segments {
	return s.transform(getAdd(pt))
}

// Returns a copy of this collection of segments with each segment mirrored about the x axis.
func (s Segments) Flip() Segments {
	return s.transform(flipPar, flipPol)
}

// Returns a copy of this collection of segments with each segment mirrored about the y axis.
func (s Segments) Flop() Segments {
	return s.transform(flopPar, flopPol)
}

// Returns a Segments with one line segment per bone in this skeleton.
func (s *Skeleton) Segments() Segments {
	out := make(Segments, 0, len(s.bones))
	for _, n := range s.order {
		b := s.bones[n]
		out = append(out, &Segment{Name: n, From: b.fromPoint(s), To: b.toPoint(s), Rot: b.rotationAngle(s)})
	}
	return out
}
