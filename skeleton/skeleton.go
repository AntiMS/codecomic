package skeleton

// Represents "rigging" for a 2-dimensional character or creature.
type Skeleton struct {
	bones map[string]*bone
	order []string
}

// Constructs a new, empty *Skeleton.
func New() *Skeleton {
	return &Skeleton{
		bones: make(map[string]*bone),
		order: make([]string, 0),
	}
}

func (s *Skeleton) copy() *Skeleton {
	bones := make(map[string]*bone)
	order := make([]string, 0, len(s.order))
	for n, b := range s.bones {
		bones[n] = &bone{from: b.from, to: b.to}
	}
	for _, o := range s.order {
		order = append(order, o)
	}
	return &Skeleton{
		bones: bones,
		order: order,
	}
}

// Merges all bones in the given skeleton into the receiver skeleton.
func (s *Skeleton) Merge(o *Skeleton) {
	for _, n := range o.order {
		s.addBone(n, o.bones[n])
	}
}
