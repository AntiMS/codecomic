package skeleton

import (
	"fmt"

	"gitlab.com/AntiMS/codecomic/point"
)

type boneSource interface {
	start(s *Skeleton) (*point.ParametricPoint, float64) // Translation, rotation angle
}

type rootBoneSource struct{}

func (bs rootBoneSource) start(s *Skeleton) (*point.ParametricPoint, float64) {
	return point.ParametricIdentity, 0
}

type endBoneSource string

func (bs endBoneSource) start(s *Skeleton) (*point.ParametricPoint, float64) {
	pb := s.bones[string(bs)]
	pt, pr := pb.from.start(s)
	return pt.Add(point.NewPolar(1, pr).Add(pb.to)).(*point.ParametricPoint), pr + pb.to.Theta()
}

type bone struct {
	from boneSource
	to   *point.PolarPoint
}

func (b *bone) fromPoint(s *Skeleton) *point.ParametricPoint {
	pt, _ := b.from.start(s)
	return pt
}
func (b *bone) toPoint(s *Skeleton) *point.ParametricPoint {
	pt, pr := b.from.start(s)
	return pt.Add(point.NewPolar(1, pr).Add(b.to)).(*point.ParametricPoint)
}
func (b *bone) rotationAngle(s *Skeleton) float64 {
	_, pr := b.from.start(s)
	return pr + b.to.Theta()
}

func (s *Skeleton) addBone(n string, b *bone) {
	_, nin := s.bones[n]
	if nin {
		panic(fmt.Sprintf("Bone %#v already present in skeleton", n))
	}
	s.bones[n] = b
	s.order = append(s.order, n)
}

// Creates a new bone from the "root point" ((0, 0) relative to the skeleton itself) to the given point.
func (s *Skeleton) BoneFromRoot(n string, p *point.PolarPoint) {
	s.addBone(n, &bone{from: rootBoneSource{}, to: p}) // Or do we want a special root boneSource rather than nil?
}

// Creates a new bone from the end of bone e to the vector sum of the endpoint of bone e and the given point.
func (s *Skeleton) BoneFromEnd(n, e string, p *point.PolarPoint) {
	s.addBone(n, &bone{from: endBoneSource(e), to: p})
}
