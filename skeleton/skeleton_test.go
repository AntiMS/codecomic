package skeleton

import (
	"fmt"
	"math"
	"testing"

	"gitlab.com/AntiMS/codecomic/point"
	"gitlab.com/AntiMS/codecomic/pose"
)

func areClose(a, e float64) bool {
	return math.Abs(a-e) < 0.00000001
}

func comparePoint(a, e point.Point) bool {
	switch ta := a.(type) {
	case *point.ParametricPoint:
		te, ok := e.(*point.ParametricPoint)
		if !ok {
			return false
		}
		return areClose(ta.X(), te.X()) && areClose(ta.Y(), te.Y())
	case *point.PolarPoint:
		te, ok := e.(*point.PolarPoint)
		if !ok {
			return false
		}
		return areClose(ta.R(), te.R()) && areClose(ta.Theta(), te.Theta())
	}
	panic(fmt.Sprintf("Unrecognized point type %#v", a))
}

func assertPoint(t *testing.T, a, e point.Point) {
	if !comparePoint(a, e) {
		t.Errorf("Comparing points: Expected %s, got %s", e.String(), a.String())
	}
}

type seg struct {
	f, t *point.ParametricPoint
	r    float64
}

func tp(r, theta float64) *point.ParametricPoint {
	return point.NewParametric(r*math.Sin(theta*point.DEGCONV), r*math.Cos(theta*point.DEGCONV))
}

func ap(p1, p2 point.Point) *point.ParametricPoint {
	return p1.ToParametric().Add(p2.ToParametric()).ToParametric()
}

func TestAll(t *testing.T) {
	dog := New()

	dog.BoneFromRoot("upper_torso", point.NewPolar(1, -90))
	dog.BoneFromRoot("lower_torso", point.NewPolar(1, 90))

	dog.BoneFromEnd("left_upper_back_leg", "lower_torso", point.NewPolar(0.25, 90))
	dog.BoneFromEnd("left_mid_back_leg", "left_upper_back_leg", point.NewPolar(0.25, 0))
	dog.BoneFromEnd("left_lower_back_leg", "left_mid_back_leg", point.NewPolar(0.5, 0))

	dog.BoneFromEnd("right_upper_back_leg", "lower_torso", point.NewPolar(0.25, 90))
	dog.BoneFromEnd("right_mid_back_leg", "right_upper_back_leg", point.NewPolar(0.25, 0))
	dog.BoneFromEnd("right_lower_back_leg", "right_mid_back_leg", point.NewPolar(0.5, 0))

	dog.BoneFromEnd("left_upper_front_leg", "upper_torso", point.NewPolar(0.25, -90))
	dog.BoneFromEnd("left_mid_front_leg", "left_upper_front_leg", point.NewPolar(0.5, 0))
	dog.BoneFromEnd("left_lower_front_leg", "left_mid_front_leg", point.NewPolar(0.25, 0))

	dog.BoneFromEnd("right_upper_front_leg", "upper_torso", point.NewPolar(0.25, -90))
	dog.BoneFromEnd("right_mid_front_leg", "right_upper_front_leg", point.NewPolar(0.5, 0))
	dog.BoneFromEnd("right_lower_front_leg", "right_mid_front_leg", point.NewPolar(0.25, 0))

	dog.BoneFromEnd("neck", "upper_torso", point.NewPolar(0.5, 0))
	dog.BoneFromEnd("totally_not_a_head", "neck", point.NewPolar(0.5, 0)) // If the name of this was (or started with) "head", it would render as a circle.
	dog.BoneFromEnd("ear", "neck", point.NewPolar(0.125, 90))             // Yeah, this dog only has one ear. Don't think too hard about it.

	dogTail := New()

	dogTail.BoneFromEnd("upper_tail", "lower_torso", point.NewPolar(0.33333, 0))
	dogTail.BoneFromEnd("mid_tail", "upper_tail", point.NewPolar(0.33333, 0))
	dogTail.BoneFromEnd("lower_tail", "mid_tail", point.NewPolar(0.33333, 0))

	dog.Merge(dogTail)

	dogPose := pose.New()

	dogPose.Add("left_upper_back_leg", point.NewPolar(1, -6))
	dogPose.Add("left_mid_back_leg", point.NewPolar(1, 10))
	dogPose.Add("left_lower_back_leg", point.NewPolar(1, -10))

	dogPose.Add("right_upper_back_leg", point.NewPolar(1, -4))
	dogPose.Add("right_mid_back_leg", point.NewPolar(1, 10))
	dogPose.Add("right_lower_back_leg", point.NewPolar(1, -10))

	dogPose.Add("left_upper_front_leg", point.NewPolar(1, -6))
	dogPose.Add("left_mid_front_leg", point.NewPolar(1, 10))
	dogPose.Add("left_lower_front_leg", point.NewPolar(1, -10))

	dogPose.Add("right_upper_front_leg", point.NewPolar(1, -4))
	dogPose.Add("right_mid_front_leg", point.NewPolar(1, 10))
	dogPose.Add("right_lower_front_leg", point.NewPolar(1, -10))

	dogPose.Add("neck", point.NewPolar(1, 45))
	dogPose.Add("totally_not_a_head", point.NewPolar(1, -45))
	// The ear is fine where it is.

	dogTailPose := pose.New()

	dogTailPose.Add("upper_tail", point.NewPolar(1, -2))
	dogTailPose.Add("mid_tail", point.NewPolar(1, -2))
	dogTailPose.Add("lower_tail", point.NewPolar(1, -2))

	dog = dog.ApplyPoses(dogPose, dogTailPose)

	segs := dog.Segments().Flip().Flop().Add(point.NewParametric(10, 10))

	exp := make(map[string]*seg)

	exp["upper_torso"] = &seg{point.NewParametric(10, 10), point.NewParametric(9, 10), 90}
	exp["lower_torso"] = &seg{point.NewParametric(10, 10), point.NewParametric(11, 10), 270}

	exp["left_upper_back_leg"] = &seg{exp["lower_torso"].t, ap(exp["lower_torso"].t, tp(0.25, 6)), 354}
	exp["left_mid_back_leg"] = &seg{exp["left_upper_back_leg"].t, ap(exp["left_upper_back_leg"].t, tp(0.25, -4)), 4}
	exp["left_lower_back_leg"] = &seg{exp["left_mid_back_leg"].t, ap(exp["left_mid_back_leg"].t, tp(0.5, 6)), 354}

	exp["right_upper_back_leg"] = &seg{exp["lower_torso"].t, ap(exp["lower_torso"].t, tp(0.25, 4)), 356}
	exp["right_mid_back_leg"] = &seg{exp["right_upper_back_leg"].t, ap(exp["right_upper_back_leg"].t, tp(0.25, -6)), 6}
	exp["right_lower_back_leg"] = &seg{exp["right_mid_back_leg"].t, ap(exp["right_mid_back_leg"].t, tp(0.5, 4)), 356}

	exp["left_upper_front_leg"] = &seg{exp["upper_torso"].t, ap(exp["upper_torso"].t, tp(0.25, 6)), 354}
	exp["left_mid_front_leg"] = &seg{exp["left_upper_front_leg"].t, ap(exp["left_upper_front_leg"].t, tp(0.5, -4)), 4}
	exp["left_lower_front_leg"] = &seg{exp["left_mid_front_leg"].t, ap(exp["left_mid_front_leg"].t, tp(0.25, 6)), 354}

	exp["right_upper_front_leg"] = &seg{exp["upper_torso"].t, ap(exp["upper_torso"].t, tp(0.25, 4)), 356}
	exp["right_mid_front_leg"] = &seg{exp["right_upper_front_leg"].t, ap(exp["right_upper_front_leg"].t, tp(0.5, -6)), 6}
	exp["right_lower_front_leg"] = &seg{exp["right_mid_front_leg"].t, ap(exp["right_mid_front_leg"].t, tp(0.25, 4)), 356}

	exp["neck"] = &seg{exp["upper_torso"].t, ap(exp["upper_torso"].t, tp(0.5, 225)), 135}
	exp["totally_not_a_head"] = &seg{exp["neck"].t, ap(exp["neck"].t, point.NewParametric(-0.5, 0)), 90}
	exp["ear"] = &seg{exp["neck"].t, ap(exp["neck"].t, tp(0.125, 135)), 225}

	exp["upper_tail"] = &seg{exp["lower_torso"].t, ap(exp["lower_torso"].t, tp(0.33333, 92)), 268}
	exp["mid_tail"] = &seg{exp["upper_tail"].t, ap(exp["upper_tail"].t, tp(0.33333, 94)), 266}
	exp["lower_tail"] = &seg{exp["mid_tail"].t, ap(exp["mid_tail"].t, tp(0.33333, 96)), 264}

	if len(exp) != len(segs) {
		t.Errorf("Expected %d segments, but got %d", len(exp), len(segs))
	}

	for _, as := range segs {
		es, ok := exp[as.Name]
		if !ok {
			t.Errorf("Expected a segment named %#v but found no such segment", as.Name)
		}
		assertPoint(t, as.From, es.f)
		assertPoint(t, as.To, es.t)
		// These for loops just normalize the rotation to within the range of [0, 360)
		for as.Rot < 0 {
			as.Rot += 360
		}
		for as.Rot >= 360 {
			as.Rot -= 360
		}
		if !areClose(as.Rot, es.r) {
			t.Errorf("Expected a rotation of %f, but got %f", es.r, as.Rot)
		}
	}
}
