package onerow

import (
	"image"
)

type oneRow struct{}

func (_ oneRow) ImageInfo(sizes ...struct{ X, Y int }) (image.Rectangle, []image.Rectangle) {
	width, height := 0, 0
	out := make([]image.Rectangle, 0, len(sizes))
	for _, s := range sizes {
		out = append(out, image.Rect(width, 0, width+s.X, s.Y))
		width += s.X
		if height < s.Y {
			height = s.Y
		}
	}
	return image.Rect(0, 0, width, height), out
}

// Satisfies the layout.Layout interface. Arranges frames horizontally in a single row.
var OneRow oneRow = oneRow{}
