package layout

import (
	"image"
)

// Manages handling positioning of frames on an image.
type Layout interface {
	// Returns the size of the whole canvas followed by a list of rectangles for frames.
	ImageInfo(sizes ...struct{ X, Y int }) (image.Rectangle, []image.Rectangle)
}
