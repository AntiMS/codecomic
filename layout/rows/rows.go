package rows

import (
	"fmt"
	"image"

	"gitlab.com/AntiMS/codecomic/layout"
)

type rowsLayout struct {
	width uint
}

func (r *rowsLayout) ImageInfo(sizes ...struct{ X, Y int }) (image.Rectangle, []image.Rectangle) {
	width, roww, prevh, curh := 0, 0, 0, 0
	out := make([]image.Rectangle, 0, len(sizes))
	for _, s := range sizes {
		if uint(s.X) >= r.width {
			panic(fmt.Sprintf("Frame width %d wider than layout width %d", s.X, r.width))
		}
		out = append(out, image.Rect(roww, prevh, roww+s.X, prevh+s.Y))
		roww += s.X
		if roww > width {
			width = roww
		}
		if curh < s.Y {
			curh = s.Y
		}
		if uint(roww+s.X) > r.width {
			prevh += curh
			roww = 0
			curh = 0
		}
	}
	return image.Rect(0, 0, width, prevh+curh), out
}

// Creates a new layout.Layout instance which arranges frames in rows, limiting the width of the total image to the given number of pixels.
func New(width uint) layout.Layout {
	return &rowsLayout{width: width}
}
