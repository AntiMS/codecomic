package main

import (
	"fmt"
	"image/png"
	"io"
	"os"

	"gitlab.com/AntiMS/codecomic/dsl/runtime"
	"gitlab.com/AntiMS/codecomic/resources"
	"gitlab.com/AntiMS/codecomic/resources/assets"
	"gitlab.com/AntiMS/codecomic/resources/path"
)

func main() {
	if len(os.Args) != 3 {
		panic("Two arguments, the name of the script to run and the name of the output file, are accepted.")
	}

	var err error

	infn := os.Args[1]
	var in io.ReadCloser
	if infn == "-" {
		in = os.Stdin
	} else {
		in, err = os.Open(infn)
		if err != nil {
			panic(fmt.Sprintf("Could not read input file %#v\n", infn))
		}
	}

	resources.AddSourcesToDefault(path.WorkingDirectorySource, &assets.AssetsSource{})

	img := runtime.Run(in)

	err = in.Close()
	if err != nil {
		panic(err)
	}

	outfn := os.Args[2]
	var out io.WriteCloser
	if outfn == "-" {
		out = os.Stdout
	} else {
		out, err = os.Create(outfn)
		if err != nil {
			panic(err)
		}
	}

	err = png.Encode(out, img)
	if err != nil {
		panic(err)
	}

	err = out.Close()
	if err != nil {
		panic(err)
	}
}
